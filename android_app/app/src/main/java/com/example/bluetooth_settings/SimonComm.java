package com.example.bluetooth_settings;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class SimonComm {

    private static final String TAG = "SIMON_COMM";
    private static final String SIMON_BLE_NAME = "ESP_GATTS_DEMO";
    private static final int REQUEST_ENABLE_BT = 6;
    private BluetoothAdapter _bluetoothAdapter;
    private BluetoothDevice _espDevice;
    private BluetoothGatt _espGatt;
    private BluetoothGattService _espServiceA;
    private Context _context;
    AppCompatActivity _app;
    //private LeDeviceListAdapter leDeviceListAdapter;
    private Handler handler;
    private TimerTask _timerTask;
    private int _currentGameRound;
    private int _currentDispRate;
    private int _semcount = 0;
    private SeekBar _seekBar;
    private Timer _timer;
    private boolean _timerStarted = false;

    public enum SimonButtons {
        BUTTON_INVALID,
        BUTTON_GREEN,
        BUTTON_RED,
        BUTTON_YELLOW,
        BUTTON_BLUE,
    }

    public enum BluetoothCommand {
        CMD_INVALID,
        CMD_GAME_USER_FINISHED,
        CMD_GAME_SEQ_DATA,
        CMD_GAME_ROUND_PROCEED,
        CMD_SET_DISP_RATE,
        CMD_SET_GAME_RATE,
        CMD_SET_RESET,
        CMD_SET_BUTTON,
        CMD_GET_DISP_RATE,
        CMD_GET_GAME_RATE,
        CMD_GET_SEQ,
        CMD_GET_ROUND,
        CMD_GET_NUM_CLIENTS,
        CMD_LAST
    }

    private class SocketData {
        public int command;
        public int dataLen;
        public byte[] buff;
    };

    public SimonComm(AppCompatActivity app) {
        //_context = context;
        _app = app;
        _seekBar = (SeekBar)_app.findViewById(R.id.seekBar);

        _timerTask = new TimerTask() {
            @Override
            public void run() {
                myTimerPop();
            }
        };

        _seekBar.setOnSeekBarChangeListener(seekListener);
        _timer = new Timer();
        SimonInit();
    }

    private void SimonInit() {
        Log.d(TAG, "---> SimonInit(): Entering");

        handler = new Handler();
        final BluetoothManager bluetoothManager =
                (BluetoothManager) _app.getSystemService(Context.BLUETOOTH_SERVICE);
        _bluetoothAdapter = bluetoothManager.getAdapter();

        if (_bluetoothAdapter == null || !_bluetoothAdapter.isEnabled()) {
            Log.d(TAG, "---> SimonInit() Bluetooth disabled.");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            _app.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        Log.d(TAG, "---> SimonInit(): Exiting");
    }

    private SeekBar.OnSeekBarChangeListener seekListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            //Log.d(TAG, "---> SeekBar(): onProgressChanged: " + i);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            Log.d(TAG, "---> SeekBar(): onStartTrackingTouch");
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            Log.d(TAG, "---> SeekBar(): onStopTrackingTouch: " + seekBar.getProgress());
            executeSetDispRate(seekBar.getProgress());
        }
    };

    public void StartScan() {
        Log.d(TAG, "---> StartScan(): Entering");
        /*
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                _bluetoothAdapter.getBluetoothLeScanner().stopScan(scanCB);
            }
        }, 2000); // in ms
        */
        _bluetoothAdapter.getBluetoothLeScanner().startScan(scanCB);
        Log.d(TAG, "---> StartScan(): Exiting");
    }

    public void StopScan() {
        Log.d(TAG, "---> StopScan(): Entering");
        _bluetoothAdapter.getBluetoothLeScanner().flushPendingScanResults(scanCB);
        _bluetoothAdapter.getBluetoothLeScanner().stopScan(scanCB);
        Log.d(TAG, "---> StopScan(): Exiting");
    }

    public void connectToServer() {
        Log.d(TAG, "---> connectToServer(): Entering");
        _espGatt = _espDevice.connectGatt(_app, true, gattCallback);

        Log.d(TAG, "---> connectToServer(): Exiting");
    }

    public void disconnectFromServer() {
        Log.d(TAG, "---> disconnectFromServer(): Entering");
        _espGatt.close();
        _espGatt.disconnect();
        Log.d(TAG, "---> disconnectFromServer(): Exiting");
    }

    public void sendButtonCmd(int id) {
        Log.d(TAG, "---> sendData(): Entering");
        Log.d(TAG, "---> sendData(): ID:" + id);
        int colorValue = 0;

        BluetoothGattCharacteristic myChar = _espServiceA.getCharacteristics().get(0);

        Log.d(TAG, "---> sendData(): My UUID: " + myChar.getUuid());
        if (myChar.getWriteType() == 0x00000002) {
            Log.d(TAG, "---> sendData(): Write Type: WRITE_TYPE_DEFAULT");
        }
        else if (myChar.getWriteType() == 0x00000004) {
            Log.d(TAG, "---> sendData(): Write Type: WRITE_TYPE_SIGNED");
        }
        else if (myChar.getWriteType() == 0x00000001) {
            Log.d(TAG, "---> sendData(): Write Type: WRITE_TYPE_NO_RESPONSE");
        }

        if (id == R.id.blue) {
            colorValue = SimonButtons.BUTTON_BLUE.ordinal();
        }
        else if (id == R.id.red) {
            colorValue = SimonButtons.BUTTON_RED.ordinal();
        }
        else if (id == R.id.yellow) {
            colorValue = SimonButtons.BUTTON_YELLOW.ordinal();
        }
        else if (id == R.id.green) {
            colorValue = SimonButtons.BUTTON_GREEN.ordinal();
        }

        SocketData sData = new SocketData();
        sData.dataLen = 4;
        sData.command = BluetoothCommand.CMD_SET_BUTTON.ordinal();
        sData.buff = intToByteArray(colorValue,false);

        Log.d(TAG, "---> sendData(): Before:" + Arrays.toString( myChar.getValue()));
        byte[] payload = socketDataToByteArray(sData);
        Log.d(TAG, "---> sendData(): After:" + Arrays.toString(payload));

        myChar.setValue(payload);

        Log.d(TAG, "---> sendData(): Sent");
        _espGatt.writeCharacteristic(myChar);
        Log.d(TAG, "---> sendData(): Exiting");
    }

    public void executeGetRound() {
        BluetoothGattCharacteristic myChar = _espServiceA.getCharacteristics().get(0);

        SocketData sData = new SocketData();
        sData.command = BluetoothCommand.CMD_GET_ROUND.ordinal();
        sData.dataLen = 0;

        byte[] payload = socketDataToByteArray(sData);
        myChar.setValue(payload);

        _espGatt.writeCharacteristic(myChar);
    }

    public void executeSetDispRate(int dispRate) {
        if (dispRate <= 0) {
            dispRate = 1;
        }

        BluetoothGattCharacteristic myChar = _espServiceA.getCharacteristics().get(0);

        SocketData sData = new SocketData();
        sData.command = BluetoothCommand.CMD_SET_DISP_RATE.ordinal();
        sData.dataLen = 1;
        sData.buff = new byte[sData.dataLen];
        sData.buff[0] = (byte)(dispRate & 0xff);

        byte[] payload = socketDataToByteArray(sData);
        myChar.setValue(payload);

        _espGatt.writeCharacteristic(myChar);

        TextView dispRateText = (TextView)_app.findViewById(R.id.dispRate);
        dispRateText.setText("Display Refresh Rate: " + dispRate + " Hz");
    }


    public void recvData() {
        BluetoothGattCharacteristic myChar = _espServiceA.getCharacteristics().get(0);
        _espGatt.readCharacteristic(myChar);
    }

    private final BluetoothGattCallback gattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    Log.d(TAG, "---> onConnectionStateChange(): Entering");
                    if (newState == BluetoothProfile.STATE_CONNECTED) {

                        Log.i(TAG, "Connected to GATT server.");
                        Log.i(TAG, "Attempting to start service discovery:" +
                                _espGatt.discoverServices());

                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        Log.i(TAG, "Disconnected from GATT server.");
                        connectToServer();
                    }
                    Log.d(TAG, "---> onConnectionStateChange(): Exiting");
                }

                @Override
                // New services discovered
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    Log.d(TAG, "---> onServicesDiscovered(): Entering");

                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Log.d(TAG, "---> onServicesDiscovered(): GATT_SUCCESS: "+ gatt.getServices());
                    } else {
                        Log.w(TAG, "onServicesDiscovered received: " + status);
                    }
                    ArrayList<BluetoothGattService> services = new ArrayList<BluetoothGattService>();
                    for (BluetoothGattService service : _espGatt.getServices()) {
                        Log.d(TAG, "---> onServicesDiscovered(): service: "+ service.getUuid() + " " + service.getUuid().getMostSignificantBits() + " " + service.getUuid().getLeastSignificantBits());
                        long msb = 0xFF00001000L;
                        long lsb = 0x800000805F9B34FBL;
                        UUID uid = new UUID(msb, lsb);
                        if ( service.getUuid().equals(uid)) {
                            Log.d(TAG, "---> onServicesDiscovered(): FOUND!: "+ service.getUuid());
                            _espServiceA = service;
                            for (BluetoothGattCharacteristic charact : _espServiceA.getCharacteristics()) {
                                Log.d(TAG, "---> onServicesDiscovered(): UUID: "+ charact.getUuid());
                            }
                            break;
                        }
                        services.add(service);
                    }

                    if (!_timerStarted) {
                        _timer.scheduleAtFixedRate(_timerTask, 2000, 1000);
                        _timerStarted = true;
                    }
                    
                    Log.d(TAG, "---> onServicesDiscovered(): Exiting");
                }

                @Override
                // Result of a characteristic read operation
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    Log.d(TAG, "---> onCharacteristicRead(): Entering");
                    final StringBuilder str = new StringBuilder();

                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Log.d(TAG, "---> onCharacteristicRead(): GATT_SUCCESS");
                        BluetoothGattCharacteristic myChar = _espServiceA.getCharacteristics().get(0);

                        for (int i = 0; i < myChar.getValue().length; i++) {
                            str.append(String.format("0x%02X, ", myChar.getValue()[i] & 0xFF));
                        }
                        handleReadResponse(myChar.getValue(), myChar.getValue().length);
                        //Log.d(TAG, "---> onCharacteristicRead(): " + Arrays.toString(myChar.getValue()));
                        Log.d(TAG, "---> onCharacteristicRead(): " + str);
                    }
                    Log.d(TAG, "---> onCharacteristicRead(): Exiting");
                    _semcount--;
                }

                @Override
                // Result of a characteristic read operation
                public void onCharacteristicWrite(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    Log.d(TAG, "---> onCharacteristicWrite(): Entering");
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Log.d(TAG, "---> onCharacteristicWrite(): GATT_SUCCESS");
                        BluetoothGattCharacteristic myChar = _espServiceA.getCharacteristics().get(0);
                        StringBuilder str = new StringBuilder();
                        for (int i = 0; i < myChar.getValue().length; i++) {
                            str.append(String.format("0x%02X, ", myChar.getValue()[i] & 0xFF));
                        }
                        //Log.d(TAG, "---> onCharacteristicRead(): " + Arrays.toString(myChar.getValue()));
                        Log.d(TAG, "---> onCharacteristicWrite(): " + str);
                    }
                    Log.d(TAG, "---> onCharacteristicWrite(): Exiting");
                    _semcount++;
                }
     };


    private void handleReadResponse(byte[] responseData, int responseLength) {
        Log.d(TAG, "---> handleReadResponse(): Entering...");
        SocketData sData = byteArrayToSocketData(responseData, responseLength);
        Log.d(TAG, "---> handleReadResponse(): Command: " + sData.command);
        if (sData.command == BluetoothCommand.CMD_GET_ROUND.ordinal()) {
            _currentGameRound = sData.buff[0];
            Log.d(TAG, "---> handleReadResponse(): Game round: " + _currentGameRound);
        }
        else if (sData.command == BluetoothCommand.CMD_GET_DISP_RATE.ordinal()) {
            _currentDispRate = sData.buff[0];
            Log.d(TAG, "---> handleReadResponse(): CMD_GET_DISP_RATE: " + _currentDispRate);
        }
        Log.d(TAG, "---> handleReadResponse(): Exiting...");
    }

    private void myTimerPop() {
        Log.d(TAG, "---> myTimerPop(): Entering...");

        if (_semcount <= 0) {
            executeGetRound();
        }

        if (_semcount > 0) {
            recvData(); // Check for data every timer pop.
        }

        // Update current game round text field on GUI
        final int lastRoundValue = _currentGameRound;
        _app.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Stuff that updates the UI
                TextView numRoundsText = (TextView)_app.findViewById(R.id.num_rounds);
                numRoundsText.setText("Current Round: " + lastRoundValue);
            }
        });

        Log.d(TAG, "---> myTimerPop(): Exiting...");
    }


    private ScanCallback scanCB = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            String deviceName = result.getDevice().getName();
            //mRecyclerViewAdapter.addDevice(result.getDevice().getAddress());
            Log.d(TAG, "---> onScanResult(): Obtained result");
            Log.d(TAG, "---> onScanResult(): Name: " +  result.getDevice().getName());
            Log.d(TAG, "---> onScanResult(): Address: " +  result.getDevice().getAddress());
            //Log.d(TAG, "---> onScanResult(): result: " +  result.getScanRecord().toString());
            //Log.d(TAG, "---> onScanResult(): " +  result.toString());
            Log.d(TAG, "---> onScanResult(): End result\n");
            if (deviceName != null && deviceName.equals(SIMON_BLE_NAME)) {
                Log.d(TAG, "---> onScanResult(): Found GATTS server! " + SIMON_BLE_NAME);
                StopScan();
                _espDevice = result.getDevice();
                Log.d(TAG, "---> onScanResult(): Before connect!");
                connectToServer();
                Log.d(TAG, "---> onScanResult(): After connect!");
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            if (errorCode == SCAN_FAILED_ALREADY_STARTED) {
                Log.d(TAG, "SCAN_FAILED_ALREADY_STARTED");
            }
            else if (errorCode == SCAN_FAILED_APPLICATION_REGISTRATION_FAILED) {
                Log.e(TAG, "SCAN_FAILED_APPLICATION_REGISTRATION_FAILED");
            }
            else if (errorCode == SCAN_FAILED_FEATURE_UNSUPPORTED) {
                Log.e(TAG, "SCAN_FAILED_FEATURE_UNSUPPORTED");
            }
            else if (errorCode == SCAN_FAILED_INTERNAL_ERROR) {
                Log.e(TAG, "SCAN_FAILED_INTERNAL_ERROR");
            }
            else {
                Log.e(TAG, "Unknown scan error");
            }
        }


        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            Log.d(TAG, "onBatchScanResults");
            super.onBatchScanResults(results);
        }


    };

    private byte[] socketDataToByteArray(SocketData sData) {
        int byteArrayLen = 8+sData.dataLen;
        byte[] payload = new byte[byteArrayLen];

        // Currently, always littleEndian
        for (int i = 0; i < byteArrayLen; i++) {
            if (i < 4) {
                // first element in struct (4 byte command)
                payload[i] = (byte)((sData.command >> ((i%4)*8)) & 0xFF);
            }
            else if (i < 8) {
                // second element in struct (4 byte data_len)
                payload[i] = (byte)((sData.dataLen >> ((i%4)*8)) & 0xFF);
            }
            else {
                // last element in struct (variable size data)
                payload[i] = sData.buff[i%sData.dataLen];
            }
        }
        return payload;
    }


    private SocketData byteArrayToSocketData(byte[] byteData, int byteLength) {
        SocketData sockData = new SocketData();
        byte[] sockBuff;

        sockData.dataLen = byteLength - 8;
        sockData.command = byteData[0];
        sockBuff = new byte[sockData.dataLen];
        for (int i = 0; i < sockData.dataLen; i++) {
            sockBuff[i] = byteData[i+8];
        }
        sockData.buff = sockBuff;
       return sockData;
    }


    private byte[] intToByteArray(int val, boolean bigEndian) {
        byte[] payload = new byte[4];

        if (!bigEndian) {
            payload[0] = (byte)((val >> (0*8)) & 0xFF);
            payload[1] = (byte)((val >> (1*8)) & 0xFF);
            payload[2] = (byte)((val >> (2*8)) & 0xFF);
            payload[3] = (byte)((val >> (3*8)) & 0xFF);
        }
        else {
            payload[3] = (byte)((val >> (0*8)) & 0xFF);
            payload[2] = (byte)((val >> (1*8)) & 0xFF);
            payload[1] = (byte)((val >> (2*8)) & 0xFF);
            payload[0] = (byte)((val >> (3*8)) & 0xFF);
        }

        return payload;
    }

}
