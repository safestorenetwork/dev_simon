package com.example.bluetooth_settings;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

public class SettingsActivity extends AppCompatActivity {

    private static final String MY_TAG = "MY_TAG";
    private SimonComm _mySimon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        /*
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();

         */
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        Log.d(MY_TAG, "Creating SimonComm");
        _mySimon = new SimonComm(this);
    }

    public void StartScanClicked(View view) {
        _mySimon.StartScan();
    }

    public void StopScanClicked(View view) {
        _mySimon.StopScan();
    }

    public void ColorClicked(View view) {
        _mySimon.sendButtonCmd(view.getId());
    }


    public void RecvDataClicked(View view) {
        _mySimon.recvData();
    }

    public void GetRoundClicked(View view) {
        _mySimon.executeGetRound();
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            //setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }
    }
}