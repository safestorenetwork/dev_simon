#ifndef COMPONENTS_SSN_WIFI_H_
#define COMPONENTS_SSN_WIFI_H_

#include "ssn_status.h"

ret_status_t ssn_wifi_init(void);
ret_status_t ssn_wifi_start_ap(void);
ret_status_t ssn_wifi_start_sta(void);
_Bool ssn_wifi_determine_host(void);
char* ssn_wifi_get_ip(void);

#endif /* COMPONENTS_SSN_WIFI_H_ */
