#ifndef SWS_COMPONENTS_INC_SSN_CLIENT_H_
#define SWS_COMPONENTS_INC_SSN_CLIENT_H_

/***************************************************************************************************
 ********************************************  INCLUDES  *******************************************
 **************************************************************************************************/
#include <sys/select.h>
#include <stdint.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "ssn_status.h"

/***************************************************************************************************
 *********************************************  DEFINES  *******************************************
 **************************************************************************************************/
#define INVALID_CLIENT_HANDLE (-1)

/***************************************************************************************************
 ********************************************  TYPEDEFS  *******************************************
 **************************************************************************************************/
typedef struct s_ssn_client {
    int                i_client_socket;
    struct sockaddr_in s_client_addr;
    socklen_t          t_addrlen;
    uint8_t            aui8_mac_addr[6];
} s_ssn_client_t, *ps_ssn_client_t;

typedef int16_t ssn_client_handle_t;

/***************************************************************************************************
 *******************************************  PROTOTYPES  ******************************************
 **************************************************************************************************/
ret_status_t ssn_client_init(uint16_t ui16_max_clients);

ret_status_t ssn_client_deinit(void);

ret_status_t ssn_client_create(ssn_client_handle_t* pt_client_handle,
                               int i_fd);

ret_status_t ssn_client_create_and_set(ssn_client_handle_t* pt_client_handle,
                                       int i_fd,
                                       struct sockaddr_in s_addr,
                                       socklen_t t_len);

ret_status_t ssn_client_remove(ssn_client_handle_t t_client_handle);

ret_status_t ssn_client_set_fd(ssn_client_handle_t t_client_handle,
                               int i_fd);

ret_status_t ssn_client_clear_fd(ssn_client_handle_t t_client_handle,
                                 int i_fd);

int ssn_client_get_fd(ssn_client_handle_t t_client_handle);

ret_status_t ssn_client_set_sockaddr(ssn_client_handle_t t_client_handle,
                                     struct sockaddr_in s_addr);

ret_status_t ssn_client_get_sockaddr(ssn_client_handle_t t_client_handle,
                                     struct sockaddr_in* ps_addr);

ret_status_t ssn_client_set_socklen(ssn_client_handle_t t_client_handle,
                                    socklen_t t_len);

ret_status_t ssn_client_set_mac_addr(ssn_client_handle_t t_client_handle,
                                     uint8_t* pui8_mac);


ret_status_t ssn_client_get_mac_addr(ssn_client_handle_t t_client_handle,
                                     uint8_t* pui8_mac);

ssn_client_handle_t ssn_client_get_handle_from_fd(int i_fd);

ssn_client_handle_t ssn_client_get_handle_from_ip(char* pac_ip_addr);

ssn_client_handle_t ssn_client_get_handle_from_sockaddr(struct sockaddr_in* ps_addr);


ret_status_t ssn_client_send(ssn_client_handle_t t_client_handle,
                             void* pv_buf,
                             uint32_t ui32_len);

ret_status_t ssn_client_send_all(void* pv_buf,
                                 uint32_t ui32_len);

fd_set* ssn_client_get_fd_set(void);

int ssn_client_get_nfds(void);

uint16_t ssn_client_get_num_clients(void);


#endif /* SWS_COMPONENTS_INC_SSN_CLIENT_H_ */
