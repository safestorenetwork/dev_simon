/***************************************************************************************************
 ********************************************  INCLUDES  *******************************************
 **************************************************************************************************/
#include <sys/select.h>
#include "ssn_socket.h"
#include "ssn_client.h"
#include "ssn_utils.h"
#include "ssn_print.h"

/***************************************************************************************************
 *********************************************  DEFINES  *******************************************
 **************************************************************************************************/
#define MAX_NUM_CLIENTS (CONFIG_LWIP_MAX_ACTIVE_TCP + 1)
#define BUFFER_SIZE     (1024)

/***************************************************************************************************
 ***************************************  STATIC PROTOTYPES  ***************************************
 **************************************************************************************************/
static int32_t process_read(int i_socket);
static void accept_thread(void* pv_context);
static void read_thread(void* pv_context);
static void upon_new_connection(ssn_client_handle_t* t_client_handle);

/***************************************************************************************************
 ****************************************  GLOBAL VARIABLES  ***************************************
 **************************************************************************************************/
static int gi_socket_fd = -1;
static void (*accept_callback)(void* pv_context);
static ret_status_t (*read_callback)(void* pv_buff, uint16_t ui16_len);

/***************************************************************************************************
 ****************************************  PUBLIC FUNCTIONS  ***************************************
 **************************************************************************************************/

ret_status_t ssn_socket_init(_Bool b_host)
{
    ssn_client_init(MAX_NUM_CLIENTS);

    if (b_host) {
        ssn_socket_setup_listener();

        TaskHandle_t t_accept_thread = NULL;
        xTaskCreate(accept_thread,
                TOSTRING(accept_thread),
                4096,
                NULL,
                tskIDLE_PRIORITY,
                &t_accept_thread);

    }
    else {
        ssn_socket_connect_to_host();
    }

    TaskHandle_t t_read_thread = NULL;
    xTaskCreate(read_thread,
                TOSTRING(read_thread),
                4096,
                NULL,
                tskIDLE_PRIORITY,
                &t_read_thread);


    return RET_OK;
}


ret_status_t ssn_socket_setup_listener(void)
{

    int i_server_socket_fd;
    int i_status;
    struct sockaddr_in server_addr = {0};

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(8000);

    i_server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (i_server_socket_fd == -1) {
        print_sys_fail("socket()");
        return -1;
    }

    i_status = bind(i_server_socket_fd, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (i_status == -1) {
        print_sys_fail("bind()");
        return -1;
    }

    i_status = listen(i_server_socket_fd, MAX_NUM_CLIENTS);
    if (i_status == -1) {
        print_sys_fail("listen()");
        return -1;
    }

    gi_socket_fd = i_server_socket_fd;

    return RET_OK;
}



ret_status_t ssn_socket_connect_to_host(void)
{
    int i_status;
    int i_server_socket_fd;
    struct sockaddr_in serv_addr;

    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(8000);

    if (inet_pton(AF_INET, "192.168.4.1", &serv_addr.sin_addr)<=0) {
        printf_red("---> %s(): Invalid address, Address not supported\n", __FUNCTION__);
        return -1;
    }

    do {
        if ((i_server_socket_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
            print_sys_fail("socket()");
            sleep(1);
            i_status = -1;
        }

        i_status = connect(i_server_socket_fd, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
        if (i_status < 0) {
            close(i_server_socket_fd);
            print_sys_fail("connect()");
            printf_red("---> %s(): Retrying...\n", __FUNCTION__);
            sleep(1);
        }

    } while (i_status < 0);

    gi_socket_fd = i_server_socket_fd;

    if (ssn_client_create_and_set(NULL, gi_socket_fd, serv_addr, 0) != 0) {
        printf_red("---> %s(): ssn_client_create_and_set() failed. [LINE = %d]\n",
            __FUNCTION__,
            __LINE__);
        return -1;
    }

    return RET_OK;
}



ret_status_t ssn_socket_register_accept_callback(void (*accept_cb)(void* pv_context))
{
    accept_callback = accept_cb;

    return RET_OK;
}



ret_status_t ssn_socket_register_read_callback(ret_status_t (*read_cb)(void* pv_buff, uint16_t ui16_len))
{
    read_callback = read_cb;

    return RET_OK;
}

/***************************************************************************************************
 ***************************************  PRIVATE FUNCTIONS  ***************************************
 **************************************************************************************************/

static void upon_new_connection(ssn_client_handle_t* t_client_handle)
{
    accept_callback((void*) t_client_handle);
}


static void accept_thread(void* pv_context)
{
    int i_listen_socket = gi_socket_fd;
    int i_incoming_socket;
    ssn_client_handle_t t_client_handle;
    struct sockaddr_in s_temp_sockaddr;
    socklen_t t_sockaddr_len;

    while (1) {
        printf_green("---> %s(): Waiting for client connections...\n", __FUNCTION__);

        t_sockaddr_len = sizeof(struct sockaddr_in);

        i_incoming_socket = accept(i_listen_socket, (struct sockaddr*)&s_temp_sockaddr, &t_sockaddr_len);
        if (i_incoming_socket == -1) {
            print_sys_fail("accept()");
            close(i_listen_socket);
            break;
        }

        printf_yellow("---> %s(): Accepted new connection from [IP = %s] [PORT = %d]\n",
            __FUNCTION__,
            inet_ntoa(s_temp_sockaddr.sin_addr),
            ntohs(s_temp_sockaddr.sin_port));

        if (ssn_client_create_and_set(&t_client_handle, i_incoming_socket, s_temp_sockaddr, t_sockaddr_len) != 0) {
            printf_red("---> %s(): ssn_client_create_and_set() failed. [LINE = %d]\n",
                __FUNCTION__,
                __LINE__);
        }

        upon_new_connection(&t_client_handle);
        
    }

}



static void read_thread(void* pv_context)
{

    uint16_t i;
    fd_set t_my_fd_set = *(ssn_client_get_fd_set());
    int i_nfds = ssn_client_get_nfds();
    int i_select_ret;
    struct timeval tv;

    tv.tv_sec = 0;
    tv.tv_usec = 10000;

    while (1) {
        /* If a new file descriptor is added to the fd_set, select needs to be re-called */
        i_select_ret = select(i_nfds, &t_my_fd_set, NULL, NULL, &tv);

        if (i_select_ret == -1) {
            print_sys_fail("select()");
        }
        else if (i_select_ret == 0) {
#if 0
            OSAL_PRINTF_BLUE("---> %s(): select() timed out. [i_nfds = %d]\n", __FUNCTION__, i_nfds);
            for (i = 0; i < i_nfds; i++) {
                if (FD_ISSET(i, &t_my_fd_set)) {
                    OSAL_PRINTF_BLUE("---> %s(): ISSET: %d\n", __FUNCTION__, i);
                }
            }
#endif
        }
        else {

            printf_cyan("---> %s(): %d client(s) have sent me data\n",
                __FUNCTION__,
                i_select_ret);

            for (i = 0; i < i_nfds; i++) {
                if (FD_ISSET(i, &t_my_fd_set)) {
                    process_read(i);
                }
            }
        }

        /* Update select values for the next select call */
        i_nfds = ssn_client_get_nfds();
        t_my_fd_set = *(ssn_client_get_fd_set());
        tv.tv_sec = 0;
        tv.tv_usec = 10000;

    }

}


static int32_t process_read(int i_socket)
{
    ssize_t t_read_val;
    uint8_t ui8_buff[BUFFER_SIZE];

    ssn_client_handle_t t_client_handle = ssn_client_get_handle_from_fd(i_socket);

    t_read_val = recv(i_socket, ui8_buff, sizeof(ui8_buff), 0);

    if (t_read_val == -1) {
        print_sys_fail("recv()");
        close(i_socket);
        ssn_client_remove(t_client_handle);
    }
    else if (t_read_val == 0) {
        /* Socket performed an orderly shutdown */

        struct sockaddr_in client_sockaddr;
        ssn_client_get_sockaddr(t_client_handle, &client_sockaddr);

        printf_yellow("---> %s(): Client disconnected. [ADDR = %s] [PORT = %d]\n",
            __FUNCTION__,
            inet_ntoa(client_sockaddr.sin_addr),
            client_sockaddr.sin_port);

        close(i_socket);
        ssn_client_remove(t_client_handle);
    }
    else {
        printf_yellow("---> %s(): Reading %d bytes:\n", __FUNCTION__, t_read_val);
        utils_hex_dump(ui8_buff, t_read_val);
        read_callback((void*)ui8_buff, (uint16_t)t_read_val);
    }

    return 0;
}
