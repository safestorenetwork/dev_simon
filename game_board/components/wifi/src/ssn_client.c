/***************************************************************************************************
 ********************************************  INCLUDES  *******************************************
 **************************************************************************************************/
#include "ssn_client.h"
#include "ssn_print.h"
#include "ssn_utils.h"
/***************************************************************************************************
 *********************************************  DEFINES  *******************************************
 **************************************************************************************************/
#define CLIENT_INDEX_CHECK(ndx)     if (((ndx + 1) > gui16_max_clients) || ndx < 0) {\
                                        return INVALID_CLIENT_HANDLE;\
                                    }

/***************************************************************************************************
 ***************************************  STATIC PROTOTYPES  ***************************************
 **************************************************************************************************/
static int16_t get_first_open_slot(void);

/***************************************************************************************************
 ****************************************  GLOBAL VARIABLES  ***************************************
 **************************************************************************************************/
static ps_ssn_client_t*    gaps_clients = NULL;
static SemaphoreHandle_t   gt_send_mutex = NULL;
static fd_set              gt_fd_set;
static int                 gi_nfds = 1;
static uint16_t            gui16_max_clients = 0;
static uint16_t            gui16_client_hwm = 0;
static uint16_t            gui16_num_clients = 0;

/***************************************************************************************************
 ****************************************  PUBLIC FUNCTIONS  ***************************************
 **************************************************************************************************/

/******************************************************************************\
| Function:      ssn_client_init()
|
| Purpose:       To initialize the ssn client API.
|
| Processing:    (1) Dynamically allocates memory for internal data structures.
|                (2) Sets up an internal fd_set for use with select() calls.
|                (3) Sets appropriate instance variables.
|
| Params:        ui16_max_clients :: The maximum number of clients expected.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         The ssn client API is meant to only have one instance
|                at one point in time -- meaning, before calling this function
|                again, it must be in a uninitialized state.
*******************************************************************************/
ret_status_t ssn_client_init(uint16_t ui16_max_clients)
{
    /* Check if we are already initialized */
    if (gui16_max_clients != 0) {
        return RET_OK;
    }

    if (gaps_clients == NULL) {
        gaps_clients = calloc(ui16_max_clients, sizeof(ps_ssn_client_t));
        NULL_CHECK(gaps_clients, -1);
    }

    FD_ZERO(&gt_fd_set);

    gui16_max_clients = ui16_max_clients;
    gi_nfds = 1;

    if (gt_send_mutex == NULL) {
        gt_send_mutex = xSemaphoreCreateMutex();
        NULL_CHECK(gt_send_mutex, -1);
    }

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_deinit()
|
| Purpose:       To uninitialize the ssn client API.
|
| Processing:    (1) Frees previously allocated memory for internal data structures.
|                (2) Resets instance variables.
|
| Params:        void
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_deinit(void)
{
    int16_t i;

    /* Remove every client, and free all dynamically allocated memory */

    for (i = 0; i < gui16_client_hwm + 1; i++) {
        if (gaps_clients[i] != NULL) {
            ssn_client_remove(i);
        }
    }

    if (gaps_clients != NULL) {
        free(gaps_clients);
        gaps_clients = NULL;
    }

    vSemaphoreDelete(gt_send_mutex);

    gt_send_mutex = NULL;
    gui16_max_clients = 0;
    gi_nfds = 1;
    gui16_num_clients = 0;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_create()
|
| Purpose:       To add a new ssn client.
|
| Processing:    (1) Finds the first open slot in memory to allocate the new client.
|                (2) Sets the given socket fd to the internal fd_set for select().
|
| Params:        pt_client_handle :: If non-null, the handle is saved at this pointer.
|                i_fd             :: The socket fd associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         It is recommended to use ssn_client_create_and_set() instead
|                of this function.
*******************************************************************************/
ret_status_t ssn_client_create(ssn_client_handle_t* pt_client_handle,
                               int i_fd)
{
    NULL_CHECK(gaps_clients, -1);

    int16_t i16_index = get_first_open_slot();

    if (i16_index < 0) {
        printf_red("---> %s(): Too many clients!\n", __FUNCTION__);
        return -1;
    }

    if (gaps_clients[i16_index] == NULL) {
        gaps_clients[i16_index] = malloc(sizeof(s_ssn_client_t));
        NULL_CHECK(gaps_clients[i16_index], -1);
        memset(gaps_clients[i16_index], 0, sizeof(s_ssn_client_t));
    }

    if (pt_client_handle != NULL) {
        *pt_client_handle = (ssn_client_handle_t)i16_index;
    }

    ssn_client_set_fd(i16_index, i_fd);

    if (i16_index > gui16_client_hwm) {
        gui16_client_hwm = i16_index;
    }

    gui16_num_clients++;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_create_and_set()
|
| Purpose:       To add a new ssn client and save its associated sockaddr.
|
| Processing:    (1) Calls ssn_client_create()
|                (2) Sets the given socket fd to the internal fd_set for select().
|
| Params:        pt_client_handle :: If non-null, the handle is saved at this pointer.
|                i_fd             :: The socket fd associated with the client.
|                s_addr           :: The sockaddr associated with the client.
|                t_len            :: The socklen associated with the sockaddr.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         This function is a short-cut to calling the following three
|                functions in-order: ssn_client_create(), ssn_client_set_sockaddr(),
|                and, ssn_client_set_socklen().
*******************************************************************************/
ret_status_t ssn_client_create_and_set(ssn_client_handle_t* pt_client_handle,
                                       int i_fd,
                                       struct sockaddr_in s_addr,
                                       socklen_t t_len)
{
    ssn_client_handle_t t_handle;
    ssn_client_handle_t* pt_handle = &t_handle;

    /* Allow pt_client_handle to be NULL if caller doesn't want handle */
    if (pt_client_handle != NULL) {
        pt_handle = pt_client_handle;
    }

    if (ssn_client_create(pt_handle, i_fd) != 0) {
        return -1;
    }

    ssn_client_set_sockaddr(*pt_handle, s_addr);
    ssn_client_set_socklen(*pt_handle, t_len);

    printf_blue("---> %s(): Added ssn Client [HANDLE = %d] [FD = %d] [IP = %s] [PORT = %d]\n",
            __FUNCTION__,
            t_handle, i_fd,
            inet_ntoa(gaps_clients[*pt_handle]->s_client_addr.sin_addr),
            ntohs(gaps_clients[*pt_handle]->s_client_addr.sin_port));

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_remove()
|
| Purpose:       To remove a client.
|
| Processing:    (1) Removes the associated socket fd with the fd_set for select()
|                (2) Frees all previously allocated memory for the client.
|
| Params:        t_client_handle  :: The client handle for the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_remove(ssn_client_handle_t t_client_handle)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients, -1);

    printf_cyan("---> %s(): Removing client. [HANDLE = %d] [FD = %d]\n",
            __FUNCTION__,
            t_client_handle,
            gaps_clients[t_client_handle]->i_client_socket);

    ssn_client_clear_fd(t_client_handle, gaps_clients[t_client_handle]->i_client_socket);

    if (gaps_clients[t_client_handle] != NULL) {
        free(gaps_clients[t_client_handle]);
        gaps_clients[t_client_handle] = NULL;
    }

    gui16_num_clients--;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_set_fd()
|
| Purpose:       To set the socket fd associated with a client for select() calls.
|
| Processing:    (1) Adds the given socket fd to the fd_set for select().
|                (2) Updates internal nfds for select().
|
| Params:        t_client_handle  :: The client handle for the client.
|                i_fd             :: The socket fd associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_set_fd(ssn_client_handle_t t_client_handle,
                               int i_fd)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    FD_SET(i_fd, &gt_fd_set);

    if ((i_fd + 1) > gi_nfds) {
        gi_nfds = i_fd + 1;
    }

    gaps_clients[t_client_handle]->i_client_socket = i_fd;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_clear_fd()
|
| Purpose:       To clear the socket fd associated with a client for select() calls.
|
| Processing:    (1) Removes the given socket fd from the fd_set for select().
|
| Params:        t_client_handle  :: The client handle for the client.
|                i_fd             :: The socket fd associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_clear_fd(ssn_client_handle_t t_client_handle,
                                 int i_fd)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    FD_CLR(i_fd, &gt_fd_set);

    gaps_clients[t_client_handle]->i_client_socket = -1;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_get_fd()
|
| Purpose:       To get the current socket fd associated with a client.
|
| Processing:    (1) Retrieves the socket fd from internal data structure.
|
| Params:        t_client_handle  :: The client handle for the client.
|
| Return:        The socket fd associated with given client
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
int ssn_client_get_fd(ssn_client_handle_t t_client_handle)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    return gaps_clients[t_client_handle]->i_client_socket;
}



/******************************************************************************\
| Function:      ssn_client_set_sockaddr()
|
| Purpose:       To set the sockaddr associated with a client.
|
| Processing:    (1) Sets the given sockaddr to the given client.
|
| Params:        t_client_handle :: The client handle for the client.
|                s_addr          :: The sockaddr associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_set_sockaddr(ssn_client_handle_t t_client_handle,
                                     struct sockaddr_in s_addr)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    gaps_clients[t_client_handle]->s_client_addr = s_addr;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_get_sockaddr()
|
| Purpose:       To get the sockaddr associated with a client.
|
| Processing:    (1) Retrieves the sockaddr from internal data structure.
|
| Params:        t_client_handle :: The client handle for the client.
|                s_addr          :: The sockaddr associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_get_sockaddr(ssn_client_handle_t t_client_handle,
                                     struct sockaddr_in* ps_addr)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    *ps_addr = gaps_clients[t_client_handle]->s_client_addr;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_set_socklen()
|
| Purpose:       To set the socklen associated with a client.
|
| Processing:    (1) Sets the given socklen to the given client.
|
| Params:        t_client_handle :: The client handle for the client.
|                t_len           :: The socklen associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
ret_status_t ssn_client_set_socklen(ssn_client_handle_t t_client_handle,
                                    socklen_t t_len)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    gaps_clients[t_client_handle]->t_addrlen = t_len;

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_set_mac_addr()
|
| Purpose:       To set the MAC address associated with a client.
|
| Processing:    (1) Sets the given MAC address to the given client.
|
| Params:        t_client_handle :: The client handle for the client.
|                pui8_mac        :: The MAC address associated with the client.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         pui8_mac must contain 6 bytes.
*******************************************************************************/
ret_status_t ssn_client_set_mac_addr(ssn_client_handle_t t_client_handle,
                                     uint8_t* pui8_mac)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);
    NULL_CHECK(pui8_mac, -1);

    memcpy(gaps_clients[t_client_handle]->aui8_mac_addr,
            pui8_mac,
            sizeof(gaps_clients[t_client_handle]->aui8_mac_addr));

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_get_mac_addr()
|
| Purpose:       To get the MAC address associated with a client.
|
| Processing:    (1) Retrieves the MAC address from internal data structure.
|
| Params:        t_client_handle :: The client handle for the client.
|                pui8_mac        :: A pointer to put the client's MAC address.
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         pui8_mac must be pre-allocated with at least 6 bytes.
*******************************************************************************/
ret_status_t ssn_client_get_mac_addr(ssn_client_handle_t t_client_handle,
                                     uint8_t* pui8_mac)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);
    NULL_CHECK(pui8_mac, -1);

    memcpy(pui8_mac,
            gaps_clients[t_client_handle]->aui8_mac_addr,
            sizeof(gaps_clients[t_client_handle]->aui8_mac_addr));

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_get_handle_from_fd()
|
| Purpose:       To get the client handle associated with the given socket fd.
|
| Processing:    (1) Searches through an internal data structure for a matching
|                    client.
|
| Params:        i_fd :: The socket fd associated with the client.
|
| Return:        Returns the ssn_client_handle_t associated with the
|                given socket fd, or INVALID_CLIENT_HANDLE if none were found.
|
| Side Effects:  None
|
| Notes:         This is an O(n) operation, with n being the current
|                high-water-mark for number of added clients.
*******************************************************************************/
ssn_client_handle_t ssn_client_get_handle_from_fd(int i_fd)
{
    NULL_CHECK(gaps_clients, -1);

    int16_t i;
    for (i = 0; i < gui16_client_hwm + 1; i++) {
        if ((gaps_clients[i] != NULL) &&
                (gaps_clients[i]->i_client_socket == i_fd)) {
            return (ssn_client_handle_t)i;
        }
    }

    return INVALID_CLIENT_HANDLE;
}



/******************************************************************************\
| Function:      ssn_client_get_handle_from_ip()
|
| Purpose:       To get the client handle associated with the given IP address.
|
| Processing:    (1) Searches through an internal data structure for a matching
|                    client.
|
| Params:        pac_ip_addr :: The IP address associated with the client.
|
| Return:        Returns the ssn_client_handle_t associated with the
|                given IP address, or INVALID_CLIENT_HANDLE if none were found.
|
| Side Effects:  None
|
| Notes:         This is at worst case, an O(n^2)
|                operation, with n being the current high-water-mark for
|                number of added clients.
*******************************************************************************/
ssn_client_handle_t ssn_client_get_handle_from_ip(char* pac_ip_addr)
{
    NULL_CHECK(pac_ip_addr, -1);
    NULL_CHECK(gaps_clients, -1);

    int16_t i;

    for (i = 0; i < gui16_client_hwm + 1; i++) {
        if ((gaps_clients[i] != NULL) &&
                (strncmp(inet_ntoa(gaps_clients[i]->s_client_addr.sin_addr), pac_ip_addr, strlen(pac_ip_addr)) == 0)) {
            return (ssn_client_handle_t)i;
        }
    }

    return INVALID_CLIENT_HANDLE;
}



/******************************************************************************\
| Function:      ssn_client_get_handle_from_sockaddr()
|
| Purpose:       To get the client handle associated with the sockaddr.
|
| Processing:    (1) Searches through an internal data structure for a matching
|                    client.
|
| Params:        ps_addr :: The sockaddr struct associated with the client.
|
| Return:        Returns the ssn_client_handle_t associated with the
|                given sockaddr, or INVALID_CLIENT_HANDLE if none were found.
|
| Side Effects:  None
|
| Notes:         This is at worst case, an O(n*sizeof(ps_addr->sin_addr))
|                operation, with n being the current high-water-mark for
|                number of added clients.
*******************************************************************************/
ssn_client_handle_t ssn_client_get_handle_from_sockaddr(struct sockaddr_in* ps_addr)
{
    NULL_CHECK(ps_addr, -1);
    NULL_CHECK(gaps_clients, -1);

    int16_t i;

    for (i = 0; i < gui16_client_hwm + 1; i++) {
        if ((gaps_clients[i] != NULL) &&
                (memcmp((void*)&gaps_clients[i]->s_client_addr.sin_addr, (void*)&ps_addr->sin_addr, sizeof(ps_addr->sin_addr)) == 0) &&
                (gaps_clients[i]->s_client_addr.sin_port == ps_addr->sin_port)) {
            return (ssn_client_handle_t)i;
        }
    }

    return INVALID_CLIENT_HANDLE;
}



/******************************************************************************\
| Function:      ssn_client_send()
|
| Purpose:       To send arbitrary data to a client.
|
| Processing:    (1) Locks a mutex for the send() call.
|                (2) Sends data to client using its socket fd.
|                (3) Unlocks the send() mutex.
|
| Params:        t_client_handle :: The client handle for the client.
|                pv_buf          :: Data to send to client
|                ui32_len        :: Length of data to send in bytes
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         pv_buf must be pre-allocated with at least ui32_len bytes.
*******************************************************************************/
ret_status_t ssn_client_send(ssn_client_handle_t t_client_handle,
                             void* pv_buf,
                             uint32_t ui32_len)
{
    CLIENT_INDEX_CHECK(t_client_handle);
    NULL_CHECK(gaps_clients[t_client_handle], -1);

    ssize_t t_send_ret;

    xSemaphoreTake(gt_send_mutex, portMAX_DELAY);

    printf_blue("---> %s(): Sending %d bytes to [HANDLE = %d] [IP = %s] [FD = %d] [PORT = %d]\n",
            __FUNCTION__,
            ui32_len,
            t_client_handle,
            inet_ntoa(gaps_clients[t_client_handle]->s_client_addr.sin_addr),
            gaps_clients[t_client_handle]->i_client_socket,
            ntohs(gaps_clients[t_client_handle]->s_client_addr.sin_port));

    t_send_ret = send(gaps_clients[t_client_handle]->i_client_socket, pv_buf, ui32_len, 0);

    xSemaphoreGive(gt_send_mutex);

    if (t_send_ret == -1) {
        print_sys_fail("send()");
        return -1;
    }
    else {
        if (gaps_clients[t_client_handle] == NULL) {
            printf_blue("---> %s(): Nevermind... that client's session is dead. [HANDLE = %d]\n",
                    __FUNCTION__,
                    t_client_handle);
        }
        else {
            printf_blue("---> %s(): Sent %d bytes to [IP = %s]\n",
                    __FUNCTION__,
                    t_send_ret,
                    inet_ntoa(gaps_clients[t_client_handle]->s_client_addr.sin_addr));
        }
    }

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_send_all()
|
| Purpose:       To send arbitrary data to every client.
|
| Processing:    (1) Iterates through every client
|                (2) Calls ssn_client_send() on each client.
|
| Params:        pv_buf          :: Data to send to all clients
|                ui32_len        :: Length of data to send in bytes
|
| Return:        Function returns its status in the form of a ret_status_t.
|                Anything other than 0 is considered a failure.
|
| Side Effects:  None
|
| Notes:         pv_buf must be pre-allocated with at least ui32_len bytes.
*******************************************************************************/
ret_status_t ssn_client_send_all(void* pv_buf,
                                 uint32_t ui32_len)
{
    NULL_CHECK(gaps_clients, -1);

    uint16_t i;

    for (i = 0; i < gui16_client_hwm + 1; i++) {
        if (gaps_clients[i] != NULL) {
            if (ssn_client_send(i, pv_buf, ui32_len) != 0) {
                return -1;
            }
        }
    }

    return RET_OK;
}



/******************************************************************************\
| Function:      ssn_client_get_fd_set()
|
| Purpose:       To get the internal fd_set for select().
|
| Processing:    (1) Retrieves the fd_set from instance variable.
|
| Params:        void
|
| Return:        Returns a pointer to the internal fd_set.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
fd_set* ssn_client_get_fd_set(void)
{
    return &gt_fd_set;
}



/******************************************************************************\
| Function:      ssn_client_get_nfds()
|
| Purpose:       To get the internal nfds variable for select().
|
| Processing:    (1) Retrieves the nfds from instance variable.
|
| Params:        void
|
| Return:        Returns a copy of the internal nfds.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
int ssn_client_get_nfds(void)
{
    return gi_nfds;
}



uint16_t ssn_client_get_num_clients(void)
{
    return gui16_num_clients;
}


/***************************************************************************************************
 ***************************************  PRIVATE FUNCTIONS  ***************************************
 **************************************************************************************************/

/******************************************************************************\
| Function:      get_first_open_slot()
|
| Purpose:       To get the first open memory slot inside the internal data structure.
|
| Processing:    (1) Iterates through the internal data structure.
|
| Params:        void
|
| Return:        Returns the first open position as an int16_t, otherwise -1 if
|                there are no more positions available.
|
| Side Effects:  None
|
| Notes:         None
*******************************************************************************/
static int16_t get_first_open_slot(void)
{
    int16_t i;

    for (i = 0; i < gui16_max_clients; i++) {
        if (gaps_clients[i] == NULL) {
            return i;
        }
    }

    return -1;
}
