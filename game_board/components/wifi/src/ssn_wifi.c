#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "ssn_wifi.h"
#include "ssn_print.h"
#include "ssn_utils.h"

#define EXAMPLE_ESP_WIFI_SSID      "SIMON_HOST"
#define EXAMPLE_ESP_WIFI_PASS      "8sjf3$@!L"
#define EXAMPLE_ESP_MAXIMUM_RETRY  50
#define EXAMPLE_MAX_STA_CONN       10

#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

static void event_handler(void* arg, esp_event_base_t event_base,
                          int32_t event_id,
                          void* event_data);
static void nvs_init();


static const char *TAG = "wifi";
static EventGroupHandle_t s_wifi_event_group;
static int s_retry_num = 0;
static char gac_ip_addr[16];
static char gac_ssid[33];
static _Bool gb_wifi_ready = 0;

ret_status_t ssn_wifi_init(void)
{
    nvs_init();

    s_wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();

    ESP_ERROR_CHECK(esp_event_loop_create_default());

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL));

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    return RET_OK;
}



ret_status_t ssn_wifi_start_ap(void)
{
    gb_wifi_ready = 1;

    wifi_config_t wifi_config = {
        .ap = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .ssid_len = strlen(EXAMPLE_ESP_WIFI_SSID),
            .password = EXAMPLE_ESP_WIFI_PASS,
            .max_connection = EXAMPLE_MAX_STA_CONN,
            .authmode = WIFI_AUTH_WPA_WPA2_PSK
        },
    };

    if (strlen(EXAMPLE_ESP_WIFI_PASS) == 0) {
        wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_AP, &wifi_config));

    ESP_LOGI(TAG, "wifi_init_softap finished. SSID:%s password:%s",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    return RET_OK;
}


ret_status_t ssn_wifi_start_sta(void)
{
    gb_wifi_ready = 1;

    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
            .pmf_cfg = {
                .capable = true,
                .required = false
            },
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );


    ESP_LOGI(TAG, "wifi_init_sta finished.");

    esp_wifi_connect();
    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }

    ESP_ERROR_CHECK(esp_event_handler_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler));
    ESP_ERROR_CHECK(esp_event_handler_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler));
    vEventGroupDelete(s_wifi_event_group);

    return RET_OK;
}


_Bool ssn_wifi_determine_host(void)
{
    uint16_t i;
    uint16_t ui16_ap_count  = 50;
    bool b_found_host = false;
    wifi_scan_config_t s_my_scans;
    wifi_mode_t e_current_mode;
    wifi_ap_record_t* as_ap_info = malloc(ui16_ap_count * sizeof(wifi_ap_record_t));

    esp_wifi_get_mode(&e_current_mode);
    memset(as_ap_info, 0, ui16_ap_count * sizeof(wifi_ap_record_t));
    memset(&s_my_scans, 0, sizeof(wifi_scan_config_t));

    /* Must be in STA Mode to scan */
    if (e_current_mode != WIFI_MODE_STA) {
        wifi_config_t wifi_config;
        memset(&wifi_config, 0, sizeof(wifi_config));
        ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
        ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    }

    s_my_scans.show_hidden = false;
    s_my_scans.scan_type = WIFI_SCAN_TYPE_ACTIVE;
    s_my_scans.scan_time.passive = 100;
    s_my_scans.scan_time.active.min = 50;
    s_my_scans.scan_time.active.max = 200;

    printf_white("---> %s(): Starting AP scan.\n", __FUNCTION__);

    ESP_ERROR_CHECK(esp_wifi_scan_start(&s_my_scans, true));
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&ui16_ap_count, as_ap_info));

    printf_white("Found %d Access Points\n", ui16_ap_count);

    for (i = 0; i < ui16_ap_count; i++) {
        /* Look for SSID name that matches another game board */
        if (strncmp(EXAMPLE_ESP_WIFI_SSID, (char*)as_ap_info[i].ssid, sizeof(EXAMPLE_ESP_WIFI_SSID) - 1) == 0) {
            memcpy(gac_ssid, (char*)as_ap_info[i].ssid, strlen((char*)as_ap_info[i].ssid) + 1);
            b_found_host = true;
            break;
        }
    }

    if (as_ap_info != NULL) {
        free(as_ap_info);
        as_ap_info = NULL;  
    }

    return !b_found_host;
}



char* ssn_wifi_get_ip(void)
{
    return gac_ip_addr;
}


static void nvs_init(void)
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
}


static void event_handler(void* arg, esp_event_base_t event_base,
                          int32_t event_id,
                          void* event_data)
{
    if (!gb_wifi_ready) return;

    if (event_id == WIFI_EVENT_AP_STACONNECTED) {
        wifi_event_ap_staconnected_t* event = (wifi_event_ap_staconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" join, AID=%d",
                 MAC2STR(event->mac), event->aid);
    } else if (event_id == WIFI_EVENT_AP_STADISCONNECTED) {
        wifi_event_ap_stadisconnected_t* event = (wifi_event_ap_stadisconnected_t*) event_data;
        ESP_LOGI(TAG, "station "MACSTR" leave, AID=%d",
                 MAC2STR(event->mac), event->aid);
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;

        strcpy(gac_ip_addr, ip4addr_ntoa(&event->ip_info.ip));

        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}
