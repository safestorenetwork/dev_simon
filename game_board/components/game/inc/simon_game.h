#ifndef COMPONENTS_SIMON_GAME_PLATFORM_H_
#define COMPONENTS_SIMON_GAME_PLATFORM_H_

#include "ssn_status.h"


ret_status_t simon_game_init(void);
ret_status_t simon_game_start(_Bool b_host);
ret_status_t simon_game_reset(void);
uint8_t simon_game_get_round(void);
ret_status_t simon_game_input_cb(uint8_t ui8_button);
void simon_game_share(void* pv_context);
void simon_game_receive(void* pv_buff,
                        uint16_t ui16_len);
void simon_game_user_finished(void);
void simon_game_seq_data(void* pv_data,
                         uint16_t ui16_data_len);
void simon_game_round_proceed(void);

#endif /* COMPONENTS_SIMON_GAME_PLATFORM_H_ */
