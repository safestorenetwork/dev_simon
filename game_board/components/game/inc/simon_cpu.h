#ifndef COMPONENTS_SIMON_CPU_PLATFORM_H_
#define COMPONENTS_SIMON_CPU_PLATFORM_H_

#include "ssn_status.h"

#define MAX_SIMON_ROUNDS 64

ret_status_t simon_cpu_demo(void);
ret_status_t simon_cpu_button_event(uint8_t ui8_button);
ret_status_t simon_cpu_play(uint8_t ui8_count);
ret_status_t simon_cpu_get_sequence(uint8_t** ppui8_buff,
                                    uint8_t* pui8_len);
ret_status_t simon_cpu_gen_sequence(void);
void simon_cpu_receive(void* pv_buff,
                       uint16_t ui16_len);

#endif /* COMPONENTS_SIMON_CPU_PLATFORM_H_ */
