#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "simon_game.h"
#include "simon_cpu.h"
#include "simon_buttons.h"
#include "ssn_print.h"
#include "ssn_utils.h"
#include "simon_buzzer.h"
#include "ssn_socket.h"
#include "ssn_client.h"
#include "simon_seven_seg.h"
#include "ssn_cmd_proc.h"

static void simon_game_thread(void* pv_context);
static void handle_user_input(void);
static void handle_loss(void);
static void handle_win(void);
static void check_for_round_proceed(void);

static uint8_t gui8_current_round = 0;
static uint8_t gui8_cur_seq_in_round = 0;
static uint8_t gui8_num_players_finished = 0;

static SemaphoreHandle_t gt_input_sem = NULL;
static SemaphoreHandle_t gt_data_received = NULL;
static SemaphoreHandle_t gt_round_proceed = NULL;

_Bool gb_host = 0;

ret_status_t simon_game_init(void)
{
    simon_cpu_demo();

    ssn_socket_register_accept_callback(simon_game_share);

    if (!gt_data_received) {
        gt_data_received = xSemaphoreCreateBinary();

        if (!gt_data_received) {
            printf_red("---> %s(): xSemaphoreCreateBinary() failed.\n", __FUNCTION__);
        }
    }
    if (!gt_round_proceed) {
        gt_round_proceed = xSemaphoreCreateBinary();

        if (!gt_round_proceed) {
            printf_red("---> %s(): xSemaphoreCreateBinary() failed.\n", __FUNCTION__);
        }
    }

    return RET_OK;
}



ret_status_t simon_game_start(_Bool b_host)
{
    gb_host = b_host;

    if (!b_host) {
        // wait to receive data from host
        printf_cyan("---> %s(): Waiting for host...\n", __FUNCTION__);
        xSemaphoreTake(gt_data_received, portMAX_DELAY);
        printf_cyan("---> %s(): Host gave me data. Now starting\n", __FUNCTION__);
    }

    gt_input_sem = xSemaphoreCreateCounting(MAX_SIMON_ROUNDS, 0);
    if (!gt_input_sem) {
        printf_red("---> %s(): xSemaphoreCreateCounting() failed.\n", __FUNCTION__);
        return RET_FAIL;
    }

    TaskHandle_t t_simon_game_thread = NULL;
    xTaskCreate(simon_game_thread,
                TOSTRING(simon_cpu_thread),
                2048,
                NULL,
                tskIDLE_PRIORITY,
                &t_simon_game_thread);

    return RET_OK;
}



ret_status_t simon_game_reset(void)
{
    gui8_current_round = 0;
    gui8_cur_seq_in_round = 0;

    xQueueReset(gt_input_sem);

    return RET_OK;
}


uint8_t simon_game_get_round(void)
{
    return gui8_current_round;
}



ret_status_t simon_game_input_cb(uint8_t ui8_button)
{
    if (gui8_current_round - gui8_cur_seq_in_round == 0) {
        return RET_OK;
    }

    uint8_t* pui8_seq;

    simon_cpu_get_sequence(&pui8_seq, NULL);

    if (pui8_seq[gui8_cur_seq_in_round] != ui8_button) {
        printf("---> %s(): Got %d was expecting %d\n",
            __FUNCTION__,
            ui8_button,
            pui8_seq[gui8_cur_seq_in_round]);
        handle_loss();
    }
    else {
        handle_win();
    }

    return RET_OK;
}



void simon_game_share(void* pv_context)
{
    printf_magenta("---> %s(): Callback\n", __FUNCTION__);

    ssn_client_handle_t t_client_handle = *(ssn_client_handle_t*)pv_context;

    uint8_t* pui8_seq_buff = NULL;
    uint8_t ui8_size;

    simon_cpu_get_sequence(&pui8_seq_buff, &ui8_size);

    s_socket_data_t* ps_sock_data = malloc(sizeof(s_socket_data_t) + ui8_size);
    ps_sock_data->i32_command = (int32_t)CMD_GAME_SEQ_DATA;
    ps_sock_data->ui32_data_len = ui8_size;
    memcpy(ps_sock_data->ai8_data, pui8_seq_buff, ui8_size);

    ssn_client_send(t_client_handle, ps_sock_data, sizeof(s_socket_data_t) + ui8_size);
    
    free(ps_sock_data);
    ps_sock_data = NULL;
}


void simon_game_user_finished(void)
{
    // Only the host should receive this message
    // A client has finished their round
    printf("---> %s(): A player has finished their round\n", __FUNCTION__);

    gui8_num_players_finished++;

    check_for_round_proceed();
}


void simon_game_seq_data(void* pv_data, uint16_t ui16_data_len)
{
        // The sequence for the client
        uint8_t* pui8_seq_buff = NULL;
        uint8_t ui8_size;

        simon_cpu_get_sequence(&pui8_seq_buff, &ui8_size);

        memcpy(pui8_seq_buff, pv_data, ui16_data_len);

        xSemaphoreGive(gt_data_received);
}


void simon_game_round_proceed(void)
{
    // The client can proceed to the next round
    xSemaphoreGive(gt_round_proceed);
}


static void check_for_round_proceed(void)
{
    if ((gui8_num_players_finished == ssn_client_get_num_clients()) &&
        (gui8_current_round - gui8_cur_seq_in_round == 0)) {

        printf_green("---> %s(): All players has finished their rounds\n", __FUNCTION__);

        s_socket_data_t s_sock_data;
        s_sock_data.i32_command = (int32_t)CMD_GAME_ROUND_PROCEED;
        s_sock_data.ui32_data_len = 0;

        ssn_client_send_all((void*)&s_sock_data, sizeof(s_socket_data_t));

        xSemaphoreGive(gt_round_proceed);
        gui8_num_players_finished = 0;
    }
}


static void handle_win(void)
{
    printf_green("---> %s(): Passed\n", __FUNCTION__);
    
    gui8_cur_seq_in_round++;

    xSemaphoreGive(gt_input_sem);

    printf("---> %s(): sem: %d, curr_round: %d\n", __FUNCTION__, gui8_current_round - gui8_cur_seq_in_round, gui8_current_round);
}



static void handle_loss(void)
{
    printf_red("---> %s(): Player lost\n", __FUNCTION__);
    
    simon_buzzer_play_fail();

    sleep(1);

    simon_game_reset();

    xSemaphoreGive(gt_input_sem);
}



static void simon_game_thread(void* pv_context)
{
    while (1) {

        // round end
        
        gui8_current_round++;
        simon_seven_seg_set_value(gui8_current_round);
        simon_cpu_play(gui8_current_round);
        handle_user_input();

        gui8_cur_seq_in_round = 0;

        sleep(1);
    }
}


static void handle_user_input(void)
{
    int i;

    for (i = 0; i < gui8_current_round; i++) {
        xSemaphoreTake(gt_input_sem, portMAX_DELAY);
        simon_seven_seg_set_value(gui8_current_round - gui8_cur_seq_in_round);
    }

    // tell host that user has finished its round
    if (!gb_host) {
        s_socket_data_t s_sock_data;
        s_sock_data.i32_command = (int32_t)CMD_GAME_USER_FINISHED;
        s_sock_data.ui32_data_len = 0;
        ssn_client_send_all((void*)&s_sock_data, sizeof(s_socket_data_t));

    }
    else {
        check_for_round_proceed();
    }

    xSemaphoreTake(gt_round_proceed, portMAX_DELAY);

    printf("---> %s(): Moving onto next round...\n", __FUNCTION__);
}