#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "simon_cpu.h"
#include "simon_buttons.h"
#include "ssn_utils.h"
#include "ssn_print.h"
#include "esp_system.h"

static uint32_t gui32_seed;


static uint8_t gaui8_seq_path[MAX_SIMON_ROUNDS];
static uint8_t gui8_num_seq;


ret_status_t simon_cpu_demo(void)
{
    for (int i = 0; i < NUM_BUTTONS; i++) {
        simon_cpu_button_event(i);
        usleep(200000);
    }
    usleep(300000);

    gui32_seed = esp_random();
    simon_cpu_gen_sequence();

    return RET_OK;
}



ret_status_t simon_cpu_button_event(uint8_t ui8_button)
{
    simon_button_event(ui8_button, 1);
    usleep(500000);
    simon_button_event(ui8_button, 0);

    return RET_OK;
}



ret_status_t simon_cpu_gen_sequence(void)
{
    srand(gui32_seed);

    int i;
    printf_white("---> %s(): Generating sequence:\n", __FUNCTION__);
    for (i = 0; i < MAX_SIMON_ROUNDS; i++) {
        gaui8_seq_path[i] = rand() % (3 + 1);
        printf("%d, ", gaui8_seq_path[i]);
    }
    putchar('\n');

    gui8_num_seq = MAX_SIMON_ROUNDS;

    return RET_OK;
}

ret_status_t simon_cpu_get_sequence(uint8_t** ppui8_buff,
                                    uint8_t* pui8_len)
{
    NULL_CHECK(ppui8_buff, RET_FAIL);

    *ppui8_buff = gaui8_seq_path;

    if (pui8_len) {
        *pui8_len = gui8_num_seq;
    }

    return RET_OK;
}


ret_status_t simon_cpu_play(uint8_t ui8_round)
{

    int i;
    // start on round 0, it should still play something
    
    for (i = 0; i < ui8_round; i++) {
        simon_button_event(gaui8_seq_path[i], 1);
        sleep(1);
        simon_button_event(gaui8_seq_path[i], 0);
      
        usleep(200000);
    }

    return RET_OK;
}

