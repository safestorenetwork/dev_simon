#ifndef COMPONENTS_SSN_CMD_PROC_H_
#define COMPONENTS_SSN_CMD_PROC_H_
/***************************************************************************************************
 ********************************************  INCLUDES  *******************************************
 **************************************************************************************************/
#include "ssn_status.h"

/***************************************************************************************************
 ********************************************  TYPEDEFS  *******************************************
 **************************************************************************************************/
typedef struct s_socket_data {
    int32_t  i32_command;
    uint32_t ui32_data_len;
    int8_t   ai8_data[];
} s_socket_data_t, *ps_socket_data_t;


typedef enum e_command {
    CMD_INVALID,
    CMD_GAME_USER_FINISHED,
    CMD_GAME_SEQ_DATA,
    CMD_GAME_ROUND_PROCEED,
    CMD_SET_DISP_RATE,
    CMD_SET_GAME_RATE,
    CMD_SET_RESET,
    CMD_SET_BUTTON,
    CMD_GET_DISP_RATE,
    CMD_GET_GAME_RATE,
    CMD_GET_SEQ,
    CMD_GET_ROUND,
    CMD_GET_NUM_CLIENTS,
    CMD_LAST
} e_command_t, *pe_command_t;


/***************************************************************************************************
 *******************************************  PROTOTYPES  ******************************************
 **************************************************************************************************/

ret_status_t ssn_process_cmd(void* pv_buff,
                             uint16_t ui16_len);

ret_status_t ssn_cmd_get_response(s_socket_data_t** pps_data);

const char* ssn_process_cmd_to_string(int32_t i32_cmd);


#endif /* COMPONENTS_SSN_CMD_PROC_H_ */