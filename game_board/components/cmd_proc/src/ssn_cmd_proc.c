/***************************************************************************************************
 ********************************************  INCLUDES  *******************************************
 **************************************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "ssn_cmd_proc.h"
#include "ssn_print.h"
#include "simon_buttons.h"
#include "simon_game.h"
#include "simon_cpu.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "simon_seven_seg.h"

/***************************************************************************************************
 ***************************************  STATIC PROTOTYPES  ***************************************
 **************************************************************************************************/
static void process_invalid             (ps_socket_data_t ps_data);
static void process_game_user_finished  (ps_socket_data_t ps_data);
static void process_game_seq_data       (ps_socket_data_t ps_data);
static void process_game_round_proceed  (ps_socket_data_t ps_data);
static void process_set_disp_rate       (ps_socket_data_t ps_data);
static void process_set_game_rate       (ps_socket_data_t ps_data);
static void process_reset               (ps_socket_data_t ps_data);
static void process_set_button          (ps_socket_data_t ps_data);
static void process_get_disp_rate       (ps_socket_data_t ps_data);
static void process_get_game_rate       (ps_socket_data_t ps_data);
static void process_get_seq             (ps_socket_data_t ps_data);
static void process_get_round           (ps_socket_data_t ps_data);
static void process_get_num_clients     (ps_socket_data_t ps_data);
static void process_last                (ps_socket_data_t ps_data);

static bool check_cmd_valid(int32_t i32_cmd);
static void timer_thread(void* pv_context);
static void prepare_response(int32_t i32_cmd,
                             uint32_t ui32_response_size);

/***************************************************************************************************
 ****************************************  GLOBAL VARIABLES  ***************************************
 **************************************************************************************************/

// Associate each command with a printable string for debugging purposes.
static const char* gpac_cmd_names[] = {
    [CMD_INVALID]            = "CMD_INVALID",
    [CMD_GAME_USER_FINISHED] = "CMD_GAME_USER_FINISHED",
    [CMD_GAME_SEQ_DATA]      = "CMD_GAME_SEQ_DATA",
    [CMD_GAME_ROUND_PROCEED] = "CMD_GAME_ROUND_PROCEED",
    [CMD_SET_DISP_RATE]      = "CMD_SET_DISP_RATE",
    [CMD_SET_GAME_RATE]      = "CMD_SET_GAME_RATE",
    [CMD_SET_RESET]          = "CMD_SET_RESET",
    [CMD_SET_BUTTON]         = "CMD_SET_BUTTON",
    [CMD_GET_DISP_RATE]      = "CMD_GET_DISP_RATE",
    [CMD_GET_GAME_RATE]      = "CMD_GET_GAME_RATE",
    [CMD_GET_SEQ]            = "CMD_GET_SEQ",
    [CMD_GET_ROUND]          = "CMD_GET_ROUND",
    [CMD_GET_NUM_CLIENTS]    = "CMD_GET_NUM_CLIENTS",
    [CMD_LAST]               = "CMD_LAST",
};

// Associate each command with a process function to execute the command.
static void (*gpf_cmd_functions[])(ps_socket_data_t) = {
    [CMD_INVALID]            = process_invalid,
    [CMD_GAME_USER_FINISHED] = process_game_user_finished,
    [CMD_GAME_SEQ_DATA]      = process_game_seq_data,
    [CMD_GAME_ROUND_PROCEED] = process_game_round_proceed,
    [CMD_SET_DISP_RATE]      = process_set_disp_rate,
    [CMD_SET_GAME_RATE]      = process_set_game_rate,
    [CMD_SET_RESET]          = process_reset,
    [CMD_SET_BUTTON]         = process_set_button,
    [CMD_GET_DISP_RATE]      = process_get_disp_rate,
    [CMD_GET_GAME_RATE]      = process_get_game_rate,
    [CMD_GET_SEQ]            = process_get_seq,
    [CMD_GET_ROUND]          = process_get_round,
    [CMD_GET_NUM_CLIENTS]    = process_get_num_clients,
    [CMD_LAST]               = process_last,
};

static ps_socket_data_t gps_response = NULL;
static TaskHandle_t gt_timer_task;

/***************************************************************************************************
 ****************************************  PUBLIC FUNCTIONS  ***************************************
 **************************************************************************************************/

ret_status_t ssn_process_cmd(void* pv_buff,
                             uint16_t ui16_len)
{
    s_socket_data_t* ps_sock_data = (s_socket_data_t*)pv_buff;

    printf_blue("---> %s(): Received Command: %d aka [%s]\n",
        __func__,
        ps_sock_data->i32_command,
        ssn_process_cmd_to_string(ps_sock_data->i32_command));

    if (!check_cmd_valid(ps_sock_data->i32_command)) {
        return RET_FAIL; 
    }

    gpf_cmd_functions[ps_sock_data->i32_command](ps_sock_data);

    return RET_OK;
}


ret_status_t ssn_cmd_get_response(s_socket_data_t** pps_data)
{
    if (!gps_response || !pps_data) {
        return RET_FAIL;
    }

    *pps_data = gps_response;
    //memcpy(s_data, gps_response, sizeof(*gps_response) + gps_response->ui32_data_len);

    return RET_OK;
}


const char* ssn_process_cmd_to_string(int32_t i32_cmd)
{
    if (!check_cmd_valid(i32_cmd)) {
        return "INVALID";
    }

    return gpac_cmd_names[i32_cmd];
}


/***************************************************************************************************
 ***************************************  PRIVATE FUNCTIONS  ***************************************
 **************************************************************************************************/

static void process_game_user_finished(ps_socket_data_t ps_data)
{
    simon_game_user_finished();
}



static void process_game_seq_data(ps_socket_data_t ps_data)
{
    simon_game_seq_data(ps_data->ai8_data, ps_data->ui32_data_len);
}



static void process_game_round_proceed(ps_socket_data_t ps_data)
{
    simon_game_round_proceed();
}



static void process_invalid(ps_socket_data_t ps_data)
{
    printf_red("%s\n", __func__);
}


static void process_set_disp_rate(ps_socket_data_t ps_data)
{
    uint8_t ui8_new_rate = (uint8_t)ps_data->ai8_data[0];

    simon_seven_set_refresh_rate(ui8_new_rate);
}


static void process_set_game_rate(ps_socket_data_t ps_data)
{

}


static void process_reset(ps_socket_data_t ps_data)
{

}


static void process_set_button(ps_socket_data_t ps_data)
{
    int32_t i32_color = *(int32_t*)ps_data->ai8_data;

    if (!i32_color || i32_color > NUM_BUTTONS) {
        printf_red("---> %s(): Invalid button selection.\n", __func__);
    }

    if (!gt_timer_task) {
        xTaskCreatePinnedToCore(timer_thread, "timer_thread", 4096, (void*)i32_color, 4, &gt_timer_task, 0);
    }

}


static void process_get_disp_rate(ps_socket_data_t ps_data)
{
    uint8_t ui8_disp_rate = simon_seven_get_refresh_rate();

    prepare_response(ps_data->i32_command, sizeof(ui8_disp_rate));

    gps_response->ai8_data[0] = ui8_disp_rate;
}


static void process_get_game_rate(ps_socket_data_t ps_data)
{

}


static void process_get_seq(ps_socket_data_t ps_data)
{

}


static void process_get_round(ps_socket_data_t ps_data)
{
    prepare_response(ps_data->i32_command, sizeof(uint8_t));

    gps_response->ai8_data[0] = simon_game_get_round();
}


static void process_get_num_clients(ps_socket_data_t ps_data)
{

}


static void process_last(ps_socket_data_t ps_data)
{

}


static bool check_cmd_valid(int32_t i32_cmd)
{
    if (i32_cmd < 0 || i32_cmd >= CMD_LAST) {
        return false;
    }

    return true;
}


static void timer_thread(void* pv_context)
{
    int32_t i32_color = (int32_t)pv_context;

    simon_cpu_button_event(i32_color-1);

    gt_timer_task = NULL;
    vTaskDelete(NULL);
}



static void prepare_response(int32_t i32_cmd,
                             uint32_t ui32_response_size)
{
    uint32_t ui32_total_size = sizeof(s_socket_data_t) + ui32_response_size;
    if (gps_response) {
        free(gps_response);
        gps_response = NULL;
    }

    if (!gps_response) {
        gps_response = malloc(ui32_total_size);
        memset(gps_response, 0, ui32_total_size);
    }

    gps_response->i32_command = i32_cmd;
    gps_response->ui32_data_len = ui32_response_size;
}