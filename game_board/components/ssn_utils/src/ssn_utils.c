#include "ssn_utils.h"



void utils_hex_dump(void* pv_buff,
                    uint16_t ui16_len)
{
	printf("---> %s(): Start hex dump\n", __FUNCTION__);

	uint16_t i;

	for (i = 0; i < ui16_len; i++) {
	    printf("%02X", *(uint8_t*)(pv_buff + i));

	    if ((i + 1) % 8 == 0) {
	        printf("\n");
	    }
	    else {
	        printf(", ");
	    }
	}

	printf("\n---> %s(): End hex dump\n", __FUNCTION__);

}
