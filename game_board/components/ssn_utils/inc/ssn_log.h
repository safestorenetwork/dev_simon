#ifndef COMPONENTS_UTILS_SSN_LOGS_H_
#define COMPONENTS_UTILS_SSN_LOGS_H_

#define SWT_MIN_LOG_LEVEL_VALUE  0
    #define SWT_LOG_EMERG       0    /* System is unusable */
    #define SWT_LOG_ALERT       1    /* Action must be taken immediately */
    #define SWT_LOG_CRIT        2    /* Critical conditions */
    #define SWT_LOG_ERR         3    /* Error conditions */
    #define SWT_LOG_WARN        4    /* Warning conditions */
    #define SWT_LOG_NOTICE      5    /* Normal but significant condition */
    #define SWT_LOG_INFO        6    /* Informational */
    #define SWT_LOG_DEBUG       7    /* Debug-level messages */
    #define SWT_LOG_TEST_0      8    /* Test-level 0 messages */
    #define SWT_LOG_TEST_1      9    /* Test-level 1 messages */
    #define SWT_LOG_TEST_2     10    /* Test-level 2 messages */
    #define SWT_LOG_DEFAULT    11    /* Default logging level */
    #define SWT_LOG_CONT       12    /* "Continued" line of log printout (only done after a line
                                      * that had no enclosing \n) */
#define SWT_MAX_LOG_LEVEL_VALUE  12


#endif /* COMPONENTS_UTILS_SSN_LOGS_H_ */