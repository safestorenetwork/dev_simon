#ifndef COMPONENTS_UTILS_SSN_PRINT_H_
#define COMPONENTS_UTILS_SSN_PRINT_H_
#include "ssn_log.h"

#define print_sys_fail(s) printf_red("---> %s(): %s failed. [LINE = %d] [REASON = %s]\n", __FUNCTION__, s, __LINE__, strerror(errno));

/* Defines used for giving color to foreground (text). */
#define FG_BLACK          "\x1b[30m"
#define FG_RED            "\x1b[31m"
#define FG_GREEN          "\x1b[32m"
#define FG_YELLOW         "\x1b[33m"
#define FG_BLUE           "\x1b[34m"
#define FG_MAGENTA        "\x1b[35m"
#define FG_CYAN           "\x1b[36m"
#define FG_WHITE          "\x1b[37m"
#define FG_LIGHT_GRAY     "\x1b[90m"
#define FG_LIGHT_RED      "\x1b[91m"
#define FG_LIGHT_GREEN    "\x1b[92m"
#define FG_LIGHT_YELLOW   "\x1b[93m"
#define FG_LIGHT_BLUE     "\x1b[94m"
#define FG_LIGHT_MAGENTA  "\x1b[95m"
#define FG_LIGHT_CYAN     "\x1b[96m"
#define FG_LIGHT_WHITE    "\x1b[97m"
#define FG_RESET          "\x1b[39m"  // Default foreground color.

/* Defines used for giving color to background (text). */
#define BG_BLACK          "\x1b[40m"
#define BG_RED            "\x1b[41m"
#define BG_GREEN          "\x1b[42m"
#define BG_YELLOW         "\x1b[43m"
#define BG_BLUE           "\x1b[44m"
#define BG_MAGENTA        "\x1b[45m"
#define BG_CYAN           "\x1b[46m"
#define BG_WHITE          "\x1b[47m"
#define BG_LIGHT_GRAY     "\x1b[100m"
#define BG_LIGHT_RED      "\x1b[101m"
#define BG_LIGHT_GREEN    "\x1b[102m"
#define BG_LIGHT_YELLOW   "\x1b[103m"
#define BG_LIGHT_BLUE     "\x1b[104m"
#define BG_LIGHT_MAGENTA  "\x1b[105m"
#define BG_LIGHT_CYAN     "\x1b[106m"
#define BG_LIGHT_WHITE    "\x1b[107m"
#define BG_RESET          "\x1b[49m"   // Default background color.

/* Defines used for formatting text. */
#define FMT_BOLD_ON        "\x1b[1m"   // Enable foreground intensity.
#define FMT_BOLD_OFF       "\x1b[21m"  // Disable foreground intensity.
#define FMT_ITALIC_ON      "\x1b[3m"
#define FMT_ITALIC_OFF     "\x1b[23m"
#define FMT_UNDERLINE_ON   "\x1b[4m"
#define FMT_UNDERLINE_OFF  "\x1b[24m"
#define FMT_TOTAL_RESET    "\x1b[0m"   // All attributes off.

/* osal_printf() macros for using some of the colors. */
#define printf_red(format, ...)            printf_ll(SWT_LOG_ERR, FG_RED format FG_RESET, ##__VA_ARGS__)
#define printf_light_red(format, ...)      printf_ll(SWT_LOG_ERR, FG_LIGHT_RED format FG_RESET, ##__VA_ARGS__)
#define printf_bold_red(format, ...)       printf_ll(SWT_LOG_EMERG, FMT_BOLD_ON FG_RED format FMT_TOTAL_RESET, ##__VA_ARGS__)
#define printf_green(format, ...)          printf_ll(SWT_LOG_INFO, FG_GREEN format FG_RESET, ##__VA_ARGS__)
#define printf_light_green(format, ...)    printf_ll(SWT_LOG_INFO, FG_LIGHT_GREEN format FG_RESET, ##__VA_ARGS__)
#define printf_bold_green(format, ...)     printf_ll(SWT_LOG_INFO, FMT_BOLD_ON FG_GREEN format FMT_TOTAL_RESET, ##__VA_ARGS__)
#define printf_yellow(format, ...)         printf_ll(SWT_LOG_WARN, FG_YELLOW format FG_RESET, ##__VA_ARGS__)
#define printf_light_yellow(format, ...)   printf_ll(SWT_LOG_WARN, FG_LIGHT_YELLOW format FG_RESET, ##__VA_ARGS__)
#define printf_bold_yellow(format, ...)    printf_ll(SWT_LOG_WARN, FMT_BOLD_ON FG_YELLOW format FMT_TOTAL_RESET, ##__VA_ARGS__)
#define printf_blue(format, ...)           printf_ll(SWT_LOG_INFO, FG_BLUE format FG_RESET, ##__VA_ARGS__)
#define printf_light_blue(format, ...)     printf_ll(SWT_LOG_INFO, FG_LIGHT_BLUE format FG_RESET, ##__VA_ARGS__)
#define printf_bold_blue(format, ...)      printf_ll(SWT_LOG_INFO, FMT_BOLD_ON FG_BLUE format FMT_TOTAL_RESET, ##__VA_ARGS__)
#define printf_magenta(format, ...)        printf_ll(SWT_LOG_DEBUG, FG_MAGENTA format FG_RESET, ##__VA_ARGS__)
#define printf_light_magenta(format, ...)  printf_ll(SWT_LOG_DEBUG, FG_LIGHT_MAGENTA format FG_RESET, ##__VA_ARGS__)
#define printf_bold_magenta(format, ...)   printf_ll(SWT_LOG_DEBUG, FMT_BOLD_ON FG_MAGENTA format FMT_TOTAL_RESET, ##__VA_ARGS__)
#define printf_cyan(format, ...)           printf_ll(SWT_LOG_NOTICE, FG_CYAN format FG_RESET, ##__VA_ARGS__)
#define printf_light_cyan(format, ...)     printf_ll(SWT_LOG_NOTICE, FG_LIGHT_CYAN format FG_RESET, ##__VA_ARGS__)
#define printf_bold_cyan(format, ...)      printf_ll(SWT_LOG_NOTICE, FMT_BOLD_ON FG_CYAN format FMT_TOTAL_RESET, ##__VA_ARGS__)
#define printf_white(format, ...)          printf_ll(SWT_LOG_DEFAULT, FG_WHITE format FG_RESET, ##__VA_ARGS__)

#define printf_ll(ll, format, ...)         if (ll <= SWT_CURRENT_LOG_LEVEL) printf(format, ##__VA_ARGS__)

#endif /* COMPONENTS_UTILS_SSN_PRINT_H_ */