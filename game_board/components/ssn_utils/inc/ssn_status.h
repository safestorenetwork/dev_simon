#ifndef COMPONENTS_UTILS_SSN_STATUS_H_
#define COMPONENTS_UTILS_SSN_STATUS_H_
#include <stdint.h>

#define RET_OK    (0)
#define RET_FAIL  (-1)

#define RET_FAIL_NOT_IMPLEMENTED (-2)


typedef int32_t ret_status_t;

#endif /* COMPONENTS_UTILS_SSN_STATUS_H_ */