#include "board_platform.h"
#include "simon_board.h"

ret_status_t board_platform_init(void)
{
#ifdef _BOARD_SIMON_ONE_
    return simon_board_init();
#else
    return RET_FAIL_NOT_IMPLEMENTED;
#endif
}
