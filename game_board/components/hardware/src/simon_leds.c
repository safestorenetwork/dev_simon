#include "simon_leds.h"
#include "simon_pins.h"
#include "driver/gpio.h"

static ret_status_t (*pap_button_funcs[])(uint8_t) = {
    simon_leds_green_set,
    simon_leds_red_set,
    simon_leds_yellow_set,
    simon_leds_blue_set
};


ret_status_t simon_leds_set(uint8_t ui8_button,
                            _Bool b_set)
{
    return pap_button_funcs[ui8_button](b_set);
}


ret_status_t simon_leds_green_set(uint8_t ui8_set)
{
    gpio_set_level(PIN_LED_GREEN, (int32_t)ui8_set);

    return RET_OK;
}


ret_status_t simon_leds_red_set(uint8_t ui8_set)
{
    gpio_set_level(PIN_LED_RED, (int32_t)ui8_set);

    return RET_OK;
}


ret_status_t simon_leds_yellow_set(uint8_t ui8_set)
{
    gpio_set_level(PIN_LED_YELLOW, (int32_t)ui8_set);

    return RET_OK;
}


ret_status_t simon_leds_blue_set(uint8_t ui8_set)
{
    gpio_set_level(PIN_LED_BLUE, (int32_t)ui8_set);

    return RET_OK;
}
