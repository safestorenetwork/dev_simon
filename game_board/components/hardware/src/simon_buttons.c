#include <string.h>
#include "simon_buttons.h"
#include "driver/gpio.h"
#include "periph_button.h"
#include "simon_leds.h"
#include "ssn_utils.h"
#include "ssn_print.h"
#include "simon_pins.h"
#include "simon_buzzer.h"
#include "simon_game.h"


#include <driver/adc.h>

#define _USE_ADC_BUTTON_READ_

static void button_event_handler(void* pv_context);


static void button_read_thread(void* pv_context);

static esp_periph_set_handle_t gt_set;

static uint16_t gaui16_freqs[] = {
    GREEN_FREQ,
    RED_FREQ,
    YELLOW_FREQ,
    BLUE_FREQ
};

static adc1_channel_t gae_adc_channels[] = {
                                                ADC1_CHANNEL_0,
                                                ADC1_CHANNEL_3,
                                                ADC1_CHANNEL_6,
                                                ADC1_CHANNEL_7
                                              };

ret_status_t simon_buttons_init(void)
{
#ifdef _USE_ADC_BUTTON_READ_
    TaskHandle_t t_button_thread = NULL;
    xTaskCreate(button_read_thread,
                TOSTRING(button_read_thread),
                2048,
                NULL,
                tskIDLE_PRIORITY,
                &t_button_thread);
    
    if (!t_button_thread) {
        printf_red("---> %s(): xTaskCreate() failed.\n", __FUNCTION__);
        return RET_FAIL;
    }

#else

    // Initialize peripherals management
    esp_periph_config_t periph_cfg = DEFAULT_ESP_PERIPH_SET_CONFIG();
    gt_set = esp_periph_set_init(&periph_cfg);

    // Initialize Button peripheral
    periph_button_cfg_t btn_cfg = {
        .gpio_mask = GPIO_SEL_36 | GPIO_SEL_39 | GPIO_SEL_34 | GPIO_SEL_35,
    };

    esp_periph_handle_t t_button_handle = periph_button_init(&btn_cfg);
    esp_periph_start(gt_set, t_button_handle);

    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    audio_event_iface_handle_t s_evt = audio_event_iface_init(&evt_cfg);

    audio_event_iface_set_listener(esp_periph_set_get_event_iface(gt_set), s_evt);

    TaskHandle_t t_button_thread = NULL;
    xTaskCreate(button_event_handler,
                TOSTRING(button_event_handler),
                2048,
                (void*)s_evt,
                tskIDLE_PRIORITY,
                &t_button_thread);
    
    if (!t_button_thread) {
        printf_red("---> %s(): xTaskCreate() failed.\n", __FUNCTION__);
        return RET_FAIL;
    }


#endif /* _USE_ADC_BUTTON_READ_ */
    
    printf("---> %s(): Button read thread started\n", __FUNCTION__);

    return RET_OK;
}



ret_status_t simon_button_toggle_event(uint8_t ui8_button,
                                       _Bool b_pressed)
{
    simon_button_event(ui8_button, b_pressed);

    if (b_pressed) {
        simon_game_input_cb(ui8_button);
    }

    return RET_OK;
}



ret_status_t simon_button_event(uint8_t ui8_button,
                                _Bool b_pressed)
{
    if (b_pressed) {
        simon_buzzer_set_freq(gaui16_freqs[ui8_button], 0);
    }
    else {
        simon_buzzer_stop();
    }

    simon_leds_set(ui8_button, b_pressed);

    return RET_OK;
}


void green_button_event(uint8_t val)
{
    if (val) {
        printf_green("---> %s(): Green pressed\n", __FUNCTION__);
        simon_buzzer_set_freq(GREEN_FREQ, 0); 
    }
    else {
        simon_buzzer_stop();
    }

    simon_leds_green_set(val);
}

void red_button_event(uint8_t val)
{
    if (val) {
        printf_red("---> %s(): red pressed\n", __FUNCTION__);
        simon_buzzer_set_freq(RED_FREQ, 0); 
    }
    else {
        simon_buzzer_stop();
    }
    
    simon_leds_red_set(val);
}

void yellow_button_event(uint8_t val)
{
    if (val) {
        printf_yellow("---> %s(): yellow pressed\n", __FUNCTION__);
        simon_buzzer_set_freq(YELLOW_FREQ, 0); 
    }
    else {
        simon_buzzer_stop();
    }
    
    simon_leds_yellow_set(val);
}

void blue_button_event(uint8_t val)
{
    if (val) {
        printf_blue("---> %s(): blue pressed\n", __FUNCTION__);
        simon_buzzer_set_freq(BLUE_FREQ, 0); 
    }
    else {
        simon_buzzer_stop();
    }
    
    simon_leds_blue_set(val);
}

#ifdef _USE_ADC_BUTTON_READ_
static void button_read_thread(void* pv_context)
{
    /* Configure ADC for highest sensitivity */
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_11);
    adc1_config_channel_atten(ADC1_CHANNEL_3, ADC_ATTEN_DB_11);
    adc1_config_channel_atten(ADC1_CHANNEL_6, ADC_ATTEN_DB_11);
    adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_DB_11);

    uint64_t aui64_times[NUM_BUTTONS];
    _Bool ab_states[NUM_BUTTONS];
    uint16_t aui16_adc_values[NUM_BUTTONS];

    memset(aui64_times, 0, sizeof(aui64_times));
    memset(ab_states, 0, sizeof(ab_states));
    memset(aui16_adc_values, 0, sizeof(aui16_adc_values));
    
    int i;
    while (1) {
        
        /* Get ADC value for each button */
        for (i = 0; i < NUM_BUTTONS; i++) {
            aui16_adc_values[i] = adc1_get_raw(gae_adc_channels[i]);
        }

        for (i = 0; i < NUM_BUTTONS; i++) {
            if (aui16_adc_values[i] < 4095) {
                if (!ab_states[i]) {
                    // was previously off
                    simon_button_toggle_event(i, 1);
                    printf("User pressed %d\n", i);
                }
                else {
                    //printf_white("%d hold\n", i);
                    // was on, still on
                }

                aui64_times[i] = esp_timer_get_time();
                ab_states[i] = 1;
            }        
            else if (ab_states[i]) {
            // if it was on for at least this amount of time, then turn off
                if (esp_timer_get_time() - aui64_times[i] > 80000) {
                    ab_states[i] = 0;
                    simon_button_toggle_event(i, 0);
                }
            }
        }

        usleep(10000);
    }
}

#else

static void button_event_handler(void* pv_context)
{

    audio_event_iface_handle_t evt = (audio_event_iface_handle_t) pv_context;
    audio_event_iface_msg_t msg;

    while (1) {
        audio_event_iface_listen(evt, &msg, portMAX_DELAY);

        printf_white("---> %s(): A button has been pressed\n", __FUNCTION__);


        if ((int)msg.data == PIN_BUTTON_GREEN) {
            green_button_event(msg.cmd == PERIPH_BUTTON_PRESSED ? 1 : 0);
        }
        else if ((int)msg.data == PIN_BUTTON_RED) {
            red_button_event(msg.cmd == PERIPH_BUTTON_PRESSED ? 1 : 0);
        }
        else if ((int)msg.data == PIN_BUTTON_YELLOW) {
            yellow_button_event(msg.cmd == PERIPH_BUTTON_PRESSED ? 1 : 0);
        }
        else if ((int)msg.data == PIN_BUTTON_BLUE) {
            blue_button_event(msg.cmd == PERIPH_BUTTON_PRESSED ? 1 : 0);
        }

    }

    esp_periph_set_stop_all(gt_set);
    audio_event_iface_remove_listener(esp_periph_set_get_event_iface(gt_set), evt);

}
#endif /* _USE_ADC_BUTTON_READ_ */