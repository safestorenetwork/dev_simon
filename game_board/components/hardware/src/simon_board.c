#include "ssn_print.h"
#include "simon_pins.h"
#include "simon_board.h"
#include "simon_buttons.h"
#include "simon_buzzer.h"

gpio_num_t gat_7_seg_dig_to_gpio[] = {
        [0] = PIN_7_SEG_DIG_1,
        [1] = PIN_7_SEG_DIG_2,
        [2] = PIN_7_SEG_DIG_3,
        [3] = PIN_7_SEG_DIG_4
};

gpio_num_t gat_7_seg_data_to_gpio[] = {
        [0] = PIN_7_SEG_DATA_A,
        [1] = PIN_7_SEG_DATA_B,
        [2] = PIN_7_SEG_DATA_C,
        [3] = PIN_7_SEG_DATA_D,
        [4] = PIN_7_SEG_DATA_E,
        [5] = PIN_7_SEG_DATA_F,
        [6] = PIN_7_SEG_DATA_G,
        [7] = PIN_7_SEG_DATA_DP
};

gpio_num_t gat_button_to_gpio[] = {
        [0] = PIN_BUTTON_GREEN,
        [1] = PIN_BUTTON_RED,
        [2] = PIN_BUTTON_YELLOW,
        [3] = PIN_BUTTON_BLUE, 
};

gpio_num_t gat_led_to_gpio[] = {
        [0] = PIN_LED_GREEN,
        [1] = PIN_LED_RED,
        [2] = PIN_LED_YELLOW,
        [3] = PIN_LED_BLUE, 
};



ret_status_t simon_board_init(void)
{
    uint8_t i;
#if 0
    uint8_t ui8_num_disps;
    seven_seg_get_num_disps(&ui8_num_disps);
#endif
    /* Initialize 7-seg displays */
    for (i = 0; i < NUM_7_SEG_DISPS; i++) {
        gpio_pad_select_gpio(gat_7_seg_dig_to_gpio[i]);
        ESP_ERROR_CHECK(gpio_set_direction(gat_7_seg_dig_to_gpio[i], GPIO_MODE_OUTPUT));
        ESP_ERROR_CHECK(gpio_set_level(gat_7_seg_dig_to_gpio[i], SEVEN_SEG_OFF));
    }

    /* Initialize 7-seg displays segments */
    for (i = 0; i < 8; i++) {
        gpio_pad_select_gpio(gat_7_seg_data_to_gpio[i]);
        ESP_ERROR_CHECK(gpio_set_direction(gat_7_seg_data_to_gpio[i], GPIO_MODE_OUTPUT));
        ESP_ERROR_CHECK(gpio_set_level(gat_7_seg_data_to_gpio[i], SEVEN_SEG_DATA_OFF));
    }

    /* Initialize piezo buzzer */
    gpio_pad_select_gpio(PIN_PIEZO_DATA);
    ESP_ERROR_CHECK(gpio_set_direction(PIN_PIEZO_DATA, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_level(PIN_PIEZO_DATA, 0));
    
    /* Initialize the buttons*/
    for (i = 0; i < 4; i++) {
        gpio_pad_select_gpio(gat_button_to_gpio[i]);
        ESP_ERROR_CHECK(gpio_set_direction(gat_button_to_gpio[i], GPIO_MODE_INPUT));
    }
    
    /* Initialize the button LEDs*/
    for (i = 0; i < 4; i++) {
        gpio_pad_select_gpio(gat_led_to_gpio[i]);
        ESP_ERROR_CHECK(gpio_set_direction(gat_led_to_gpio[i], GPIO_MODE_OUTPUT));
        ESP_ERROR_CHECK(gpio_set_level(gat_led_to_gpio[i], 0));
    }

    simon_buttons_init();

    simon_buzzer_init();

    return RET_OK;
}



