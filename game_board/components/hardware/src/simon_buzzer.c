#include <unistd.h>
#include "simon_buzzer.h"
#include "driver/ledc.h"
#include "simon_pins.h"
#include "ssn_print.h"
#include "esp_timer.h"

static void stop_buzzer(void*);

ret_status_t simon_buzzer_init(void)
{

    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_6_BIT, // resolution of PWM duty
        .freq_hz = (uint16_t)200,                      // frequency of PWM signal
        .speed_mode = LEDC_HIGH_SPEED_MODE,           // timer mode
        .timer_num = LEDC_TIMER_0,            // timer index
        .clk_cfg = LEDC_AUTO_CLK,              // Auto select the source clock
    };

    ledc_timer_config(&ledc_timer);

#if 0
    // Prepare and set configuration of timer1 for low speed channels
    ledc_timer.speed_mode = LEDC_HS_MODE;
    ledc_timer.timer_num = LEDC_HS_TIMER;
    ledc_timer_config(&ledc_timer);
#endif

    ledc_channel_config_t ledc_channel = {
            .channel    = LEDC_CHANNEL_0,
            .duty       = 1,
            .gpio_num   = PIN_PIEZO_DATA,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_0
    };

    ledc_channel_config(&ledc_channel);
#if 0
    TaskHandle_t t_button_thread = NULL;
    xTaskCreate(button_event_handler,
                TOSTRING(button_event_handler),
                2048,
                (void*)s_evt,
                tskIDLE_PRIORITY,
                &t_button_thread);
    
    if (!t_button_thread) {
        printf_red("---> %s(): xTaskCreate() failed.\n", __FUNCTION__);
        return RET_FAIL;
    }

    printf("---> %s(): Button read thread started\n", __FUNCTION__);
#endif

    simon_buzzer_stop();

    return RET_OK;
}


ret_status_t simon_buzzer_play_fail(void)
{
    int i;
    for (i = 800; i > 100; i--) {
        if (ledc_set_freq(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, i) != ESP_OK) {
            printf_red("---> %s(): ledc_set_freq() failed.\n", __FUNCTION__);
            return RET_FAIL;
        }
        usleep(2000);
    }
    simon_buzzer_stop();
    return RET_OK;
}


ret_status_t simon_buzzer_set_freq(uint32_t ui32_freq,
                                   uint32_t ui32_time_us)
{
    if (ledc_set_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, 1) != ESP_OK) {
        printf_red("---> %s(): ledc_set_freq() failed.\n", __FUNCTION__);
    }
    if (ledc_update_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0) != ESP_OK) {
        printf_red("---> %s(): ledc_set_freq() failed.\n", __FUNCTION__);
    }
    if (ledc_set_freq(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, ui32_freq) != ESP_OK) {
        printf_red("---> %s(): ledc_set_freq() failed.\n", __FUNCTION__);
        return RET_FAIL;
    }

    if (ui32_time_us) {
            esp_timer_handle_t i_trigger_timer = NULL;

        const esp_timer_create_args_t trigger_timer_args = {
            .callback = &stop_buzzer,
            //.dispatch_method = ESP_TIMER_TASK,
            //.arg = (void*) LCM_Stream,
        };

        if (i_trigger_timer != NULL) {
            esp_timer_stop(i_trigger_timer);
            esp_timer_delete(i_trigger_timer);
            i_trigger_timer = NULL;
        }

        esp_timer_create(&trigger_timer_args, &i_trigger_timer);

        if (esp_timer_start_once(i_trigger_timer, ui32_time_us) == ESP_OK) {
            printf_cyan("---> %s(): Timer started\n", __FUNCTION__);
        }

    }

    return RET_OK;
}


ret_status_t simon_buzzer_stop(void)
{
    stop_buzzer(NULL);

    return RET_OK;
}


static void stop_buzzer(void* pv_context)
{
    if (ledc_set_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, 0) != ESP_OK) {
        printf_red("---> %s(): ledc_set_freq() failed.\n", __FUNCTION__);
    }
    if (ledc_update_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0) != ESP_OK) {
        printf_red("---> %s(): ledc_set_freq() failed.\n", __FUNCTION__);
    }
}