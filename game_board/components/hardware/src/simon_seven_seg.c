#include <unistd.h>
#include <math.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "simon_seven_seg.h"
#include "simon_board.h"
#include "simon_pins.h"
#include "ssn_print.h"
#include "ssn_utils.h"

#define REFRESH_RATE_HZ 60

static void display_thread(void* pv_context);


static uint32_t gui32_disp_value = 0;
static uint8_t  gui8_refresh_rate_hz = REFRESH_RATE_HZ;

static uint8_t gaui8_segment_map[] = {
    [0] = 0x3F,
    [1] = 0x06,
    [2] = 0x5B,
    [3] = 0x4F,
    [4] = 0x66,
    [5] = 0x6D,
    [6] = 0x7D,
    [7] = 0x07,
    [8] = 0x7F,
    [9] = 0x6F,
    [10] = 0x77,
    [11] = 0x7C,
    [12] = 0x39,
    [13] = 0x5E,
    [14] = 0x79,
    [15] = 0x71
};


ret_status_t simon_seven_seg_init(void)
{

    TaskHandle_t t_display_thread = NULL;
    xTaskCreate(display_thread,
                TOSTRING(display_thread),
                2048,
                (void*)NULL,
                tskIDLE_PRIORITY,
                &t_display_thread);

    return RET_OK;
}


ret_status_t simon_seven_seg_write_digit(uint8_t ui8_digit,
                                         uint8_t ui8_value)
{

    int i;
    for (i = 0; i < 3; i++) {
        if (ui8_digit == i) {
            gpio_set_level(gat_7_seg_dig_to_gpio[i], SEVEN_SEG_ON);
        }
        else {
            gpio_set_level(gat_7_seg_dig_to_gpio[i], SEVEN_SEG_OFF);
        }
    }

    for (i = 0; i < (sizeof(ui8_value) * 8); i++) {

        if (gaui8_segment_map[ui8_value] & (1 << i)) {
            /* The ith bit is set... */
            gpio_set_level(gat_7_seg_data_to_gpio[i], SEVEN_SEG_DATA_ON);
        }
        else {
            gpio_set_level(gat_7_seg_data_to_gpio[i], SEVEN_SEG_DATA_OFF);
        }
    }

    return RET_OK;
}



ret_status_t simon_seven_seg_set_value(uint32_t ui32_value)
{
    gui32_disp_value = ui32_value;

    printf_white("---> %s(): Value: %d\n", __FUNCTION__, ui32_value);

    return RET_OK;
}



ret_status_t simon_seven_set_refresh_rate(uint8_t ui8_rate)
{
    printf_white("---> %s(): Value: %d Hz\n", __FUNCTION__, ui8_rate);

    gui8_refresh_rate_hz = ui8_rate;

    return RET_OK;
}



uint8_t simon_seven_get_refresh_rate(void)
{
    return gui8_refresh_rate_hz;
}



static void display_thread(void* pv_context)
{

    uint8_t ui8_digits[NUM_7_SEG_DISPS];
    memset(ui8_digits, 0, NUM_7_SEG_DISPS);

    int i;

    while (1) {
        for (i = 0; i < NUM_7_SEG_DISPS; i++) {
            ui8_digits[i] = (int)(gui32_disp_value / (pow(10,i))) % 10;
            //printf("value: %d\n", (int)(ui32_value / (pow(10,i))) % 10);
        }

        for (i = 0; i < NUM_7_SEG_DISPS; i++) {
            //printf("Writing %d to digit%d [FUNCTION = %s] [LINE = %d]\n", ui8_digits[i], NUM_SEG_DISPS - i, __FUNCTION__, __LINE__);
            simon_seven_seg_write_digit(NUM_7_SEG_DISPS - (i+1), ui8_digits[i]);

            usleep(1000000 / gui8_refresh_rate_hz / NUM_7_SEG_DISPS);
        }

    }

}