#ifndef _SIMON_BUTTONS_H_
#define _SIMON_BUTTONS_H_

#include "ssn_status.h"

#define NUM_BUTTONS (4)

typedef enum e_simon_button {
    BUTTON_INVALID,
    BUTTON_GREEN,
    BUTTON_RED,
    BUTTON_YELLOW,
    BUTTON_BLUE
} e_simon_button_t, *ps_simon_button_t;


ret_status_t simon_buttons_init(void);
ret_status_t simon_button_toggle_event(uint8_t ui8_button,
                                       _Bool b_pressed);
ret_status_t simon_button_event(uint8_t ui8_button,
                                _Bool b_pressed);
void green_button_event(uint8_t val);
void red_button_event(uint8_t val);
void yellow_button_event(uint8_t val);
void blue_button_event(uint8_t val);

#endif /* _SIMON_BUTTONS_H_ */