#ifndef _SIMON_BUZZER_H_
#define _SIMON_BUZZER_H_

#include "ssn_status.h"

#define GREEN_FREQ  (392)
#define RED_FREQ    (330)
#define YELLOW_FREQ (262)
#define BLUE_FREQ   (196)

ret_status_t simon_buzzer_init(void);
ret_status_t simon_buzzer_play_fail(void);
ret_status_t simon_buzzer_set_freq(uint32_t ui32_freq,
                                   uint32_t ui32_time_us);
ret_status_t simon_buzzer_stop(void);

#endif /* _SIMON_BUZZER_H_ */