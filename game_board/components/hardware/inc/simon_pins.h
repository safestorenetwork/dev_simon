#ifndef _SIMON_PINS_H_
#define _SIMON_PINS_H_

#include "driver/gpio.h"

/* 7-Seg display pins */
#define PIN_7_SEG_DIG_1   (GPIO_NUM_2)
#define PIN_7_SEG_DIG_2   (GPIO_NUM_4)
#define PIN_7_SEG_DIG_3   (GPIO_NUM_5)
#define PIN_7_SEG_DIG_4   (GPIO_NUM_12)

#define PIN_7_SEG_DATA_A  (GPIO_NUM_13)
#define PIN_7_SEG_DATA_B  (GPIO_NUM_14)
#define PIN_7_SEG_DATA_C  (GPIO_NUM_15)
#define PIN_7_SEG_DATA_D  (GPIO_NUM_18)
#define PIN_7_SEG_DATA_E  (GPIO_NUM_19)
#define PIN_7_SEG_DATA_F  (GPIO_NUM_21)
#define PIN_7_SEG_DATA_G  (GPIO_NUM_22)
#define PIN_7_SEG_DATA_DP (GPIO_NUM_23)

/* Piezo buzzer pin */
#define PIN_PIEZO_DATA    (GPIO_NUM_25)

/* Button pins */
#define PIN_BUTTON_GREEN  (GPIO_NUM_36)
#define PIN_BUTTON_RED    (GPIO_NUM_39)
#define PIN_BUTTON_YELLOW (GPIO_NUM_34)
#define PIN_BUTTON_BLUE   (GPIO_NUM_35)

/* LED pins */
#define PIN_LED_GREEN     (GPIO_NUM_32)
#define PIN_LED_RED       (GPIO_NUM_33)
#define PIN_LED_YELLOW    (GPIO_NUM_26)
#define PIN_LED_BLUE      (GPIO_NUM_27)

#define SEVEN_SEG_ON  ((uint32_t)0)
#define SEVEN_SEG_OFF ((uint32_t)1)

#define SEVEN_SEG_DATA_ON  ((uint32_t)0)
#define SEVEN_SEG_DATA_OFF ((uint32_t)1)

#endif /* _SIMON_PINS_H_ */