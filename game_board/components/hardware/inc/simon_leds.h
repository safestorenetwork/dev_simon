#ifndef _SIMON_LEDS_H_
#define _SIMON_LEDS_H_

#include "ssn_status.h"


ret_status_t simon_leds_set(uint8_t ui8_button,
                            _Bool b_set);
ret_status_t simon_leds_green_set(uint8_t ui8_set);
ret_status_t simon_leds_red_set(uint8_t ui8_set);
ret_status_t simon_leds_yellow_set(uint8_t ui8_set);
ret_status_t simon_leds_blue_set(uint8_t ui8_set);


#endif /* _SIMON_LEDS_H_ */