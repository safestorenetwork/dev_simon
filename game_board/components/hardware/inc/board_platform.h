#ifndef COMPONENTS_HARDWARE_INC_BOARD_PLATFORM_H_
#define COMPONENTS_HARDWARE_INC_BOARD_PLATFORM_H_

#include "ssn_status.h"

ret_status_t board_platform_init(void);

#endif /* COMPONENTS_HARDWARE_INC_BOARD_PLATFORM_H_ */
