#ifndef COMPONENTS_SIMON_SEVEN_SEG_PLATFORM_H_
#define COMPONENTS_SIMON_SEVEN_SEG_PLATFORM_H_

#include "ssn_status.h"

ret_status_t simon_seven_seg_init(void);
ret_status_t simon_seven_seg_write_digit(uint8_t ui8_digit,
                                         uint8_t ui8_value);
ret_status_t simon_seven_seg_set_value(uint32_t ui32_value);
ret_status_t simon_seven_set_refresh_rate(uint8_t ui8_rate);
uint8_t simon_seven_get_refresh_rate(void);

#endif /* COMPONENTS_SIMON_SEVEN_SEG_PLATFORM_H_*/