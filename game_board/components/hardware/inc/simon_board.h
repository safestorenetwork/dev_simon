#ifndef _SIMON_BOARD_H_
#define _SIMON_BOARD_H_

#include "driver/gpio.h"
#include "ssn_status.h"

#define NUM_7_SEG_DISPS (3)


ret_status_t simon_board_init(void);


extern gpio_num_t gat_7_seg_dig_to_gpio[];
extern gpio_num_t gat_7_seg_data_to_gpio[];
extern gpio_num_t gat_button_to_gpio[];
extern gpio_num_t gat_led_to_gpio[];


#endif /* _SIMON_BOARD_H_ */