#include <stdio.h>
#include <unistd.h>
#include "ssn_bt_cmd_proc.h"
#include "ssn_print.h"
#include "simon_buttons.h"

static void process_invalid(void);
static void process_set_disp_rate(void);
static void process_set_game_rate(void);
static void process_reset(void);
static void process_set_blue(void);
static void process_set_red(void);
static void process_set_yellow(void);
static void process_set_green(void);
static void process_get_disp_rate(void);
static void process_get_game_rate(void);
static void process_get_seq(void);
static void process_get_round(void);
static void process_get_num_clients(void);
static void process_last(void);


void (*cmd_functions[])() = {
	[BT_CMD_INVALID]         = process_invalid,
    [BT_CMD_SET_DISP_RATE]   = process_set_disp_rate,
    [BT_CMD_SET_GAME_RATE]   = process_set_game_rate,
    [BT_CMD_SET_RESET]       = process_reset,
    [BT_CMD_SET_BLUE]        = process_set_blue,
    [BT_CMD_SET_RED]         = process_set_red,
    [BT_CMD_SET_YELLOW]      = process_set_yellow,
    [BT_CMD_SET_GREEN]       = process_set_green,
    [BT_CMD_GET_DISP_RATE]   = process_get_disp_rate,
    [BT_CMD_GET_GAME_RATE]   = process_get_game_rate,
    [BT_CMD_GET_SEQ]         = process_get_seq,
    [BT_CMD_GET_ROUND]       = process_get_round,
    [BT_CMD_GET_NUM_CLIENTS] = process_get_num_clients,
    [BT_CMD_LAST]            = process_last,
};


ret_status_t ssn_bt_process_cmd(void* pv_buff,
                                uint16_t ui16_len)
{
	e_bt_command_t e_cmd = *(e_bt_command_t*)pv_buff;

    cmd_functions[e_cmd]();

    return 0;
}



static void process_invalid(void)
{
    printf("%s\n", __func__);
}


static void process_set_disp_rate(void)
{
    printf("%s\n", __func__);
}


static void process_set_game_rate(void)
{
    printf("%s\n", __func__);
}


static void process_reset(void)
{
    printf("%s\n", __func__);
}


static void process_set_blue(void)
{
    printf("%s\n", __func__);
    blue_button_event(1);
    usleep(500000);
    blue_button_event(0);
}


static void process_set_red(void)
{
    printf("%s\n", __func__);
    red_button_event(1);
    usleep(500000);
    red_button_event(0);
}


static void process_set_yellow(void)
{
    printf("%s\n", __func__);
    yellow_button_event(1);
    usleep(500000);
    yellow_button_event(0);
}


static void process_set_green(void)
{
    printf("%s\n", __func__);
    green_button_event(1);
    usleep(500000);
    green_button_event(0);
}


static void process_get_disp_rate(void)
{
    printf("%s\n", __func__);
}


static void process_get_game_rate(void)
{
    printf("%s\n", __func__);
}


static void process_get_seq(void)
{
    printf("%s\n", __func__);
}


static void process_get_round(void)
{
    printf("%s\n", __func__);
}


static void process_get_num_clients(void)
{
    printf("%s\n", __func__);
}


static void process_last(void)
{
    printf("%s\n", __func__);
}