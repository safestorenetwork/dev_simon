#ifndef COMPONENTS_SSN_BT_CMD_PROC_H_
#define COMPONENTS_SSN_BT_CMD_PROC_H_

#include "ssn_status.h"


typedef enum e_command {
    BT_CMD_INVALID,
    BT_CMD_SET_DISP_RATE,
    BT_CMD_SET_GAME_RATE,
    BT_CMD_SET_RESET,
    BT_CMD_SET_BLUE,
    BT_CMD_SET_RED,
    BT_CMD_SET_YELLOW,
    BT_CMD_SET_GREEN,
    BT_CMD_GET_DISP_RATE,
    BT_CMD_GET_GAME_RATE,
    BT_CMD_GET_SEQ,
    BT_CMD_GET_ROUND,
    BT_CMD_GET_NUM_CLIENTS,
    BT_CMD_LAST
} e_bt_command_t, *pe_bt_command_t;


ret_status_t ssn_bt_process_cmd(void* pv_buff,
                                uint16_t ui16_len);

#endif /* COMPONENTS_SSN_BT_CMD_PROC_H_ */