#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "audio_pipeline.h"
//#include "display.h"
#include "ssn_utils.h"
#include "simon_board.h"
#include "board_platform.h"
#include "simon_cpu.h"
#include "simon_game.h"
#include "ssn_socket.h"
#include "ssn_wifi.h"
#include "ssn_print.h"
#include "ssn_client.h"
#include "esp_log.h"
#include "simon_seven_seg.h"
#include "ssn_bluetooth.h"
#include "ssn_cmd_proc.h"

#define FIRMWARE_VERSION  0.1.0

static void simon_cpu_thread(void* pv_context);

void app_main()
{
    printf("Starting simon alpha version=[%s]!\n", TOSTRING(FIRMWARE_VERSION));

    if (board_platform_init() < 0) {
        printf("board_platform_init() failed\n");
    }

    simon_seven_seg_init();
    ssn_socket_register_read_callback(ssn_process_cmd);
    ssn_wifi_init();

    _Bool b_host = ssn_wifi_determine_host();
    if (b_host) {
        printf_cyan("---> %s(): I am host!\n", __FUNCTION__);
        ssn_wifi_start_ap();
        ssn_bluetooth_init();
    }
    else {
        printf_cyan("---> %s(): I am not host!\n", __FUNCTION__);
        ssn_wifi_start_sta();
    }

    simon_game_init();

    ssn_socket_init(b_host);

#if 0
    TaskHandle_t t_simon_cpu_thread = NULL;
    xTaskCreate(simon_cpu_thread,
                TOSTRING(simon_cpu_thread),
                2048,
                NULL,
                tskIDLE_PRIORITY,
                &t_simon_cpu_thread);
#endif
    simon_game_start(b_host);
    uint8_t disp = 0;
    while (1) {
        for (int i = 0; i < 0x10; i++) {
            //simon_board_write_digit(disp, i);
            sleep(1);
        }
        disp = (disp + 1) % 3;
    }

}


static void simon_cpu_thread(void* pv_context)
{
    int i = 1;

    while (1) {

        simon_cpu_play(i);
        i++;
        sleep(3);
	}
}
